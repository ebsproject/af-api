package org.ebs.services.converter;

import org.ebs.model.AfPropertyConfig;
import org.ebs.services.to.DesignDefinitionParameter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor(onConstructor=@__({@Autowired}) )
public class DesignDefinitionParameterToAfPropertyConfigConverter 
        implements Converter<DesignDefinitionParameter, AfPropertyConfig>{

    private final DesignDefinitionParameterToAfPropertyUiConverter uiConverter;

    @Override
    public AfPropertyConfig convert(DesignDefinitionParameter source) {
        AfPropertyConfig target = new AfPropertyConfig();

        target.setLayoutVariable(source.getIsLayoutVariable().asBoolean());
        target.setOrderNumber(source.getOrderNumber());
        target.setRequired(source.getRequired().asBoolean());

        target.setUi(uiConverter.convert(source));

        return target;
    }

}