package org.ebs.services.converter;

import org.ebs.model.AfPropertyRule;
import org.ebs.services.to.PropertyRule;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class AfPropertyRuleToPropertyRuleConverter implements Converter<AfPropertyRule,PropertyRule>{

    @Override
    public PropertyRule convert(AfPropertyRule source) {
        PropertyRule target = new PropertyRule();
        BeanUtils.copyProperties(source, target);

        return target;
    }

}