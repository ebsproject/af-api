package org.ebs.services.converter;

import org.ebs.model.AfPropertyUi;
import org.ebs.services.to.PropertyUi;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
class AfPropertyUiToPropertyUiConverter implements Converter<AfPropertyUi,PropertyUi> {

    @Override
    public PropertyUi convert(AfPropertyUi source) {
        PropertyUi target = new PropertyUi();
        BeanUtils.copyProperties(source, target);

        return target;
    }
    
}