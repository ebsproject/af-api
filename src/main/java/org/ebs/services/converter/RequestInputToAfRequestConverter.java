package org.ebs.services.converter;

import static org.ebs.model.CatalogProperty.CATALOG.EXPT_ID;
import static org.ebs.model.CatalogProperty.CATALOG.OCC_ID;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.ebs.model.AfProperty;
import org.ebs.model.AfRequest;
import org.ebs.model.AfRequestEntry;
import org.ebs.model.AfRequestParameter;
import org.ebs.model.repos.AfPropertyRepository;
import org.ebs.services.to.ParameterInput;
import org.ebs.services.to.RequestInput;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor(onConstructor=@__({@Autowired}) )
class RequestInputToAfRequestConverter implements Converter<RequestInput, AfRequest> {

    private final AfPropertyRepository propertyRepository;

    @Override
    public AfRequest convert(RequestInput source) {
        AfRequest target = new AfRequest();
        BeanUtils.copyProperties(source, target);

        target.setMethod(propertyRepository.findByCodeAndDeletedIsFalse(source.getMethod()).get());

        target.setInstitute(source.getOrganization_code());

        List<AfRequestParameter> params = new ArrayList<>();
        for (ParameterInput param : source.getParameters()) {
            params.addAll(makeParamValues(param));
        }

        target.setParameters(params);
        addExperimentParameters(source, target);
        params.forEach(p -> p.setRequest(target));

        long[] entryIds = source.getEntryList().getEntry_id();
        int[] entryNumbers = source.getEntryList().getEntry_number();
        target.setEntries(new ArrayList<>(entryIds.length));
        for(int i = 0; i < entryIds.length; i++) {
            target.getEntries().add(new AfRequestEntry(0, source.getExperiment_id(), entryIds[i], target, entryNumbers[i]));
        }

        return target;
    }

    /**
     * extract the value or values for a given parameter and generate one {@link AfRequestParameter} for each value
     * @param param
     * @return
     */
    List<AfRequestParameter> makeParamValues(ParameterInput param) {
        AfProperty prop = propertyRepository.findByCodeAndDeletedIsFalse(param.getCode()).orElseThrow(
            () -> new RuntimeException("invalid property: "+ param.getCode()));
        if(param.getVals() == null){
            return Arrays.asList(
                new AfRequestParameter(prop, param.getVal()));
        }
        return Arrays.stream(param.getVals())
            .map(val -> new AfRequestParameter(prop, val.toString()))
            .collect(Collectors.toList());
    }


    void addExperimentParameters(RequestInput source, AfRequest target) {

        AfRequestParameter exptIdParam = new AfRequestParameter();
        exptIdParam.setProperty(propertyRepository.findByIdAndDeletedIsFalse(EXPT_ID.id()).get());
        exptIdParam.setValue(String.valueOf(source.getExperiment_id()));

        AfProperty occIdProp = propertyRepository.findByIdAndDeletedIsFalse(OCC_ID.id()).get();

        List<AfRequestParameter> occIdParamList = source.getOccurrence_id().stream()
            .map(val -> new AfRequestParameter(occIdProp, val.toString()))
            .collect(Collectors.toList());

        target.getParameters().add(exptIdParam);
        target.getParameters().addAll(occIdParamList);
    }
}