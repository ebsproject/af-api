package org.ebs.services.converter;

import org.ebs.model.AfProperty;
import org.ebs.services.to.DesignDefinitionParameter;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class DesignDefinitionParameterToAfPropertyConverter
        implements Converter<DesignDefinitionParameter, AfProperty> {

    @Override
    public AfProperty convert(DesignDefinitionParameter source) {
        AfProperty target = new AfProperty();

        //code cannot be set with values in source
        target.setDataType(source.getDataType());
        target.setDescription(source.getDescription());
        target.setName(source.getVariableAbbrev());
        target.setVariableLabel(source.getVariableLabel());
        target.setType("input");

        return target;
    }

}