package org.ebs.services.converter;

import org.ebs.model.AfProperty;
import org.ebs.services.to.DesignDefinition;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;

/**
 * Convert a {@link DesignDefinition} into a {@link AfProperty} which will
 * represent an element in the Design Catalog
 */
@Component
@RequiredArgsConstructor
public class DesignDefinitionToAfPropertyConverter
        implements Converter<DesignDefinition, AfProperty> {

    private final DesignDefinitionMetadataToAfPropertyMetaSetConverter metaConverter;

    @Override
    public AfProperty convert(DesignDefinition source) {
        AfProperty target = new AfProperty();

        target.setType("catalog_item");
        target.setDataType("integer");
        target.setCode(source.getMetadata().getMethod());
        target.setName(source.getMetadata().getDesign());

        target.setMeta(metaConverter.convert(source.getMetadata()));
        target.getMeta().forEach(m -> m.setProperty(target));

        return target;
    }
}