package org.ebs.services.converter;

import org.ebs.model.AfRequestEntry;
import org.ebs.services.to.RequestEntry;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class AfRequestEntryToRequestEntryConverter implements Converter<AfRequestEntry, RequestEntry> {
    @Override
    public RequestEntry convert(AfRequestEntry source) {
        RequestEntry target = new RequestEntry();
        BeanUtils.copyProperties(source, target);

        return target;
    }

}