package org.ebs.services.converter;

import static java.util.stream.Collectors.toList;
import static org.ebs.services.to.Request.CUSTON_DESIGN;

import java.util.List;

import org.ebs.model.AfProperty;
import org.ebs.model.AfRequest;
import org.ebs.model.AfRequestEntry;
import org.ebs.model.AfRequestParameter;
import org.ebs.model.CatalogProperty;
import org.ebs.model.repos.AfPropertyRepository;
import org.ebs.services.to.RequestFileDesignInput;
import org.ebs.util.Utils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
public class RequestFileDesignInputToAfRequestConverter implements Converter<RequestFileDesignInput,AfRequest> {

    private final AfPropertyRepository propertyRepo;

    @Override
    public AfRequest convert(RequestFileDesignInput source) {
        AfRequest target = new AfRequest();

        Utils.copyNotNulls(source, target);
        target.setDesign(source.getDesignFileName());
        target.setEntries(generateEntries(source, target));
        target.setParameters(generateOccParams(source));
        target.getParameters().add(generateExptParam(source));
        target.setType(CUSTON_DESIGN);
        
        target.getParameters().forEach(p -> p.setRequest(target));
        
        return target;
    }

    AfRequestParameter generateExptParam(RequestFileDesignInput source) {
        AfProperty experimentId = propertyRepo.findByIdAndDeletedIsFalse(CatalogProperty.CATALOG.EXPT_ID.id()).get();
        return new AfRequestParameter(experimentId, "0");
    }

    List<AfRequestParameter> generateOccParams(RequestFileDesignInput source) {
        AfProperty paramOccId = propertyRepo.findByIdAndDeletedIsFalse(CatalogProperty.CATALOG.OCC_ID.id()).get();
                
        return source.getOccurrenceIds().stream()
            .map(o -> new AfRequestParameter(paramOccId, String.valueOf(o)))
            .collect(toList());
        
    }
    
    List<AfRequestEntry> generateEntries(RequestFileDesignInput source, AfRequest target) {
        final int[] i = {1};
        return source.getEntryIds().stream()
            .map(id -> new AfRequestEntry(0, 0, id, target, i[0]++))
            .collect(toList());
    }
}
