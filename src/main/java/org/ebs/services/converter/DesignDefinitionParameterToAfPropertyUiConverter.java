package org.ebs.services.converter;

import org.ebs.model.AfPropertyUi;
import org.ebs.services.to.DesignDefinitionParameter;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
class DesignDefinitionParameterToAfPropertyUiConverter
        implements Converter<DesignDefinitionParameter, AfPropertyUi>{

    @Override
    public AfPropertyUi convert(DesignDefinitionParameter source) {
        AfPropertyUi propertyUi = new AfPropertyUi();

        propertyUi.setCatalogue(false);
        propertyUi.setDefaultValue(source.getDefaultValue());
        propertyUi.setDisabled(source.getDisabled().asBoolean());
        propertyUi.setMaximum(source.getMaximum());
        propertyUi.setMinimum(source.getMinimum());
        propertyUi.setMultiple(false);
        propertyUi.setUnit(source.getUnit());
        propertyUi.setVisible(source.getVisible().asBoolean());

        return propertyUi;
    }

}