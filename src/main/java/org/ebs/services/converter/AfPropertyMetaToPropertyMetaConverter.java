package org.ebs.services.converter;

import org.ebs.model.AfPropertyMeta;
import org.ebs.services.to.PropertyMeta;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class AfPropertyMetaToPropertyMetaConverter implements Converter<AfPropertyMeta,PropertyMeta>{

    @Override
    public PropertyMeta convert(AfPropertyMeta source) {
        PropertyMeta target = new PropertyMeta();
        BeanUtils.copyProperties(source, target);

        return target;
    }

}