package org.ebs.services.converter;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.ebs.model.AfProperty;
import org.ebs.model.AfPropertyMeta;
import org.ebs.services.to.DesignDefinition;
import org.ebs.services.to.DesignDefinitionMetadata;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * Convert a {@link DesignDefinition} into a {@link AfProperty} which will
 * represent an element in the Design Catalog
 */
@Component
public class DesignDefinitionMetadataToAfPropertyMetaSetConverter implements Converter<DesignDefinitionMetadata, Set<AfPropertyMeta>> {

    @Override
    public Set<AfPropertyMeta> convert(DesignDefinitionMetadata source) {
        Set<AfPropertyMeta> target = Arrays.asList(DesignDefinitionMetadata.class.getDeclaredFields()).stream()
            .filter(f -> !f.isSynthetic())
            .filter(f -> !"method|design".contains(f.getName()))
            .map(f -> toPropertyMeta(f, source))
            .filter(p -> p.getValue() != null)
            .flatMap(p -> p.getCode().equals("syntax") ? streamSyntax(p) : Stream.of(p))
            .collect(Collectors.toSet());
        
        return target;
    }

    AfPropertyMeta toPropertyMeta(Field f, DesignDefinitionMetadata designDefinitionMeta) {
        String val = null;
        try {
            f.setAccessible(true);
            Object objValue = f.get(designDefinitionMeta);
            if(objValue != null)
                val = objValue.toString();
        } catch (IllegalArgumentException | IllegalAccessException ex) {
            ex.printStackTrace();
            return null;
        }
        return new AfPropertyMeta(f.getName(), val);
    }
    /**
     * Splits metadata element 'syntax' in several elements if the syntax has variants
     * splitted by pipe '|'. It also removes the original syntax element.
     * @param metadataSet the metadata set to adjust
     */
    Stream<AfPropertyMeta> streamSyntax(AfPropertyMeta syntax){
        String[] syntaxList = syntax.getValue().split("\\s*\\|\\s*");

        return syntaxList.length == 1 ? Stream.of(syntax) :
            IntStream.range(0, syntaxList.length)
                .mapToObj(i -> new AfPropertyMeta("syntax" + (i + 1), syntaxList[i]));
    }
}