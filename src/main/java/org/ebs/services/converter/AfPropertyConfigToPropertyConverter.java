package org.ebs.services.converter;

import org.ebs.model.AfPropertyConfig;
import org.ebs.services.to.Property;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
class AfPropertyConfigToPropertyConverter implements Converter<AfPropertyConfig,Property> {

    @Override
    public Property convert(AfPropertyConfig source) {
        Property target = new Property(0);
        BeanUtils.copyProperties(source, target);
       
        BeanUtils.copyProperties(source.getConfigProperty(), target);

        target.setParentPropertyId(source.getProperty().getId());

        return target;
    }
    
}