package org.ebs.services.converter;

import static java.util.Optional.ofNullable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.ebs.model.AfRequest;
import org.ebs.model.AfRequestParameter;
import org.ebs.services.to.Request;
import org.ebs.services.to.RequestMetadata;
import org.ebs.util.DateTimeCoercing;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
class AfRequestToRequestConverter implements Converter<AfRequest, Request> {

    private final AfRequestEntryToRequestEntryConverter entryConverter;

    @Override
    public Request convert(AfRequest source) {
        Request target = new Request();
        target.setCreatedBy(source.getCreatedBy());
        target.setStatus(source.getStatus());

        RequestMetadata meta = target.getMetadata();
        BeanUtils.copyProperties(source, meta);

        meta.setId( createRequestId(source));
        meta.setOrganization_code(source.getInstitute());
        //for design file requests there is no method
        meta.setMethod(ofNullable(source.getMethod()).map(m -> m.getCode()).orElse(null));
        meta.setTimestamp( DateTimeCoercing.DATETIME_FORMAT.format(
            source.getCreatedOn().toInstant()));

        setParameterValues(source, target);
        setEntryList(source, target);

        return target;

    }

    private String createRequestId(AfRequest req) {
        return req.getUuid().toString() + "_SD_0000";
    }

    /**
     * sets the value of a parameter, either if it's a single value or an array
     * @param source
     * @param target
     */
    void setParameterValues(AfRequest source, Request target) {
        Map<String,List<Object>> paramValueMap = new HashMap<>();
        for (AfRequestParameter afParam : source.getParameters()) {
            paramValueMap.putIfAbsent(afParam.getProperty().getCode(), new ArrayList<>());
            paramValueMap.get(afParam.getProperty().getCode()).add(valueForParameter(afParam));
        }

        setExptAndOccMetadata(target.getMetadata(), paramValueMap);

        paramValueMap.forEach((k,v) -> {
            
            if (v.size() > 1) {
                target.addParameter(k, v);
            } else {
                target.addParameter(k, v.get(0));
            }                
        });

    }

    Object valueForParameter(AfRequestParameter p) {
        switch (p.getProperty().getDataType()){
            case "integer": return Integer.valueOf(p.getValue());
            case "boolean": return Boolean.valueOf(p.getValue())  ? "T" : "F";
            default: return p.getValue();
        }
    }

    /**
     * Loads parameters for experimentId and occurenceIds and sets them in metadata. Then removes it from
     * parameter map to avoid exporting them as parameters
     */
    void setExptAndOccMetadata(RequestMetadata meta, Map<String,List<Object>> paramValueMap) {
        meta.setExperiment_id(Integer.parseInt( paramValueMap.get("experimentId").get(0).toString()));
        meta.setOccurrence_id( paramValueMap.get("occurrenceId").stream()
            .map(v -> Long.valueOf(v.toString()))
            .collect(Collectors.toList())
            );
        paramValueMap.remove("experimentId");
        paramValueMap.remove("occurrenceId");

    }

    void setEntryList(AfRequest source, Request target) {
        target.setEntryList( source.getEntries().stream()
            .map(e -> entryConverter.convert(e))
            .collect(Collectors.toList()));
    }
}