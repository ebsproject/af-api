package org.ebs.services.converter;

import org.ebs.model.AfRequest;
import org.ebs.services.to.BrRequest;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
class AfToBrRequestConverter implements Converter<AfRequest, BrRequest> {

    @Override
    public BrRequest convert(AfRequest source) {
        BrRequest target = new BrRequest();
        BeanUtils.copyProperties(source, target);
            
        return target;
    }
    
}