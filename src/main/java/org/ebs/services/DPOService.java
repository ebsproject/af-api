package org.ebs.services;

import java.io.IOException;

import org.ebs.services.to.EntryListInput;
import org.ebs.services.to.Request;
import org.ebs.services.to.RequestStatus;

public interface DPOService {

    RequestStatus processDesignRequest(Request request, EntryListInput entries) throws IOException;
}