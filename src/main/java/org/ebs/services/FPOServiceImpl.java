package org.ebs.services;

import static org.ebs.services.to.Request.CUSTON_DESIGN;
import static org.ebs.services.to.RequestStatus.failed;
import static org.ebs.services.to.RequestStatus.failed_aeo_status;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAccessor;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Scanner;

import org.ebs.services.to.AEOResponse;
import org.ebs.services.to.BrDesignBlockElement;
import org.ebs.services.to.BrPlot;
import org.ebs.services.to.DesignArray;
import org.ebs.services.to.Request;
import org.ebs.services.to.RequestStatus;
import org.ebs.util.DateTimeCoercing;
import org.ebs.util.RestClient;
import org.ebs.util.brapi.BrPagedResponse;
import org.ebs.util.brapi.BrapiClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
class FPOServiceImpl implements FPOService {
    private static final Logger log = LoggerFactory.getLogger(FPOServiceImpl.class);

    @Value("${ebs.fpo.target-path}")
    private String fpoTargetPath;
    @Value("${ebs.fpo.expiration-time-minutes}")
    private long expirationTimeMinutes;

    private final RequestService requestService;
    private final BrapiClient b4rApiClient;
    private final DesignArrayParser parser;
    private final RestClient aeoApiClient;

    @Override
    public void proccessDesignArray() {
        
        requestService.findByStatus(RequestStatus.submitted).forEach(req -> {
            log.trace("processing Design Array File for: {}",req.getMetadata().getId());
            if(verifyRequestStatus(req)) {
                processRequest(req);
            }
        });
    }


    @Override
    public void proccessCustomDesignArray() {
        
        requestService.findByStatus(RequestStatus.processing_custom_design).forEach(req -> {
            log.trace("processing Custom Design Array File '{}' for: {}",req.getMetadata().getDesign(), req.getMetadata().getId());
            if(verifyRequestStatus(req)) {
                processRequest(req);
            }

        });
    }

    /**
     * Checks if a request is ready for processing or not. Additionally updates status of requests failing
     * @param req
     * @return
     */
    boolean verifyRequestStatus(Request req) {
        String uuid = req.getMetadata().getId();
        if( isOldRequest(req)) {
            log.info("Marking request {} as expired", uuid);
            requestService.updateStatus(uuid, RequestStatus.expired);
            return false;
        }

        //custom designs are not checked in AEO
        if( req.getMetadata().getType().equals(CUSTON_DESIGN) ) {
            return true;
        }
        
        RequestStatus status = Optional.ofNullable(aeoApiClient.get("status/"+uuid, null, AEOResponse.class))
            .map(r -> r.getStatus())
            .map(s -> s.get(0))
            .map(s -> s.getRequestStatus())
            .orElse(failed_aeo_status);
        log.trace("Status from AEO: {}", status);

        if(status.equals(failed_aeo_status)) {
            return false;
        }

        if(status.equals(failed)) {
            requestService.updateStatus(uuid, status);
            return false;
        }

        return true;
    }

    void processRequest(Request request) {
        String uuid = request.getMetadata().getId();
        Path filePath = createPath(request);

        log.info("file path: {}", filePath);
        if (Files.exists(filePath)) {
            log.info("file found: {}", filePath);
            //Request request = requestService.findByUuid(uuid);
            try{
                DesignArray designArray = parseDesignArray(filePath, request);
                log.debug("Design Array: {}", designArray.getPlotList());
                Map<String, List<BrPlot>> plotResult = savePlots(designArray.getPlotList(), request.getCreatedBy());
                
                List<BrDesignBlockElement> designTable = parser.parseDesignTable(designArray, plotResult, request);
                log.debug("Array Table: {}", designTable);
                
                if( saveDesignTable(designTable, request.getCreatedBy()) ) {
                    requestService.updateStatus(uuid, RequestStatus.completed);
                } else {
                    requestService.updateStatus(uuid, RequestStatus.failed_b4rapi_designArray);
                }
            }catch(Exception e) {
                e.printStackTrace();
                requestService.updateStatus(uuid, RequestStatus.failed_b4rapi_plots);
            }
        } else {
            log.info("file does not exist (yet): {}", filePath.toString());
        }

    }
    /**
     * Return the path where the design array file is expected for a specific request
     * @param request with a specific type (Trial or Custom)
     * @return the Path to the result file for the given request
     */
    Path createPath(Request request) {
        if( request.getMetadata().getType().equals(CUSTON_DESIGN)) {
            return Paths.get(fpoTargetPath, request.getMetadata().getDesign());
        }

        String uuid = request.getMetadata().getId();
        String fileName = uuid.concat("_DesignArray.csv");
        Path basePath = Paths.get(fpoTargetPath, uuid);
        Path filePath = Paths.get(basePath.toString(), fileName);
        return filePath;
    }

    Map<String, List<BrPlot>> savePlots(Map<String,List<BrPlot>> plotList, String user) {

        Map<String,List<BrPlot>> resultPlots = new HashMap<>();

        plotList.forEach((occId,plots) -> {
            BrPagedResponse<BrPlot> plotResponse = b4rApiClient.withUser(user)
            .postRecords("plots", plots, BrPlot.class);
            log.trace("returning object for occ {}: {}",occId, plotResponse);
            resultPlots.putIfAbsent(occId, plotResponse.getResult().getData());
        });

        return resultPlots;
    }

    DesignArray parseDesignArray(Path filePath, Request request) {

        log.info("parsing " + filePath);
        File designArrayFile = new File(filePath.toString());
        
        DesignArray designArray = null;
        try (Scanner scanner = new Scanner(designArrayFile)){

            designArray = parser.generateDesign(scanner, request);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return designArray;
    }

    /**
     * Saves a Design Table in B4R
     * Fields required by the service: occurrenceDbId, designDbId, plotDbId, blockType, blockValue, blockLevelNumber
     * @param designTable
     */
    boolean saveDesignTable (List<BrDesignBlockElement> designTable, String user) {
        BrPagedResponse<BrDesignBlockElement> designResponse = b4rApiClient.withUser(user)
            .postRecords("experiment-designs", designTable, BrDesignBlockElement.class);
        return designResponse.getResult().getData().size() == designTable.size();
    }

    boolean isOldRequest(Request req) {
        TemporalAccessor reqTimestamp = DateTimeCoercing.DATETIME_FORMAT.parse(req.getMetadata().getTimestamp());
        return Instant.from(reqTimestamp)
            .plus(expirationTimeMinutes, ChronoUnit.MINUTES)
            .isBefore(Instant.now());
    }

}