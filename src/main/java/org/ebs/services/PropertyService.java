package org.ebs.services;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.ebs.services.to.Property;
import org.ebs.services.to.PropertyMeta;
import org.ebs.services.to.PropertyRule;
import org.ebs.services.to.PropertyUi;
import org.ebs.util.FilterInput;

public interface PropertyService {

    Optional<Property> findProperty(int propertyId, int parentPropertyId);

    Optional<Property> findPropertyByCode(String propertyCode, int parentPropertyId);

    Optional<Property> findCatalog(int catalogPropertyId, List<FilterInput> filters);

    List<Property> findCatalogElements(Property catalogProperty);

    Optional<PropertyUi> findPropertyUi(int propertyId, int configPropertyId);

    Set<PropertyRule> findPropertyRules(int propertyId, int configPropertyId);

    Set<PropertyMeta> findPropertyMeta(int propertyId);
}