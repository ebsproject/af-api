package org.ebs.services;

import java.util.List;

import org.ebs.services.to.Request;
import org.ebs.services.to.RequestFileDesignInput;
import org.ebs.services.to.RequestInput;
import org.ebs.services.to.RequestStatus;

public interface RequestService {

    Request createRequest(RequestInput requestInput);
    List<Request> findByStatus(RequestStatus status);
    Request findByUuid(String uuid);
    RequestStatus updateStatus(String uuid, RequestStatus status);

    Request createFileDesignRequest(RequestFileDesignInput requestInput);
}