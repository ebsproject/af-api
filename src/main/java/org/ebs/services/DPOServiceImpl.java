package org.ebs.services;

import static org.ebs.graphql.validation.RequestInputValidator.needsNRepInEntries;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.ebs.graphql.validation.RequestInputValidator;
import org.ebs.services.to.AEOResponse;
import org.ebs.services.to.EntryListInput;
import org.ebs.services.to.Request;
import org.ebs.services.to.RequestStatus;
import org.ebs.util.RestClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
@Slf4j
public class DPOServiceImpl implements DPOService {

    @Value("${ebs.dpo.target-path}")
    private String dpoTargetPath;

    private final ObjectMapper om;
    private final RestClient aeoApiClient;

    @Override
    public RequestStatus processDesignRequest(Request request, EntryListInput entryList)
            throws IOException {

        Path basePath = Paths.get(dpoTargetPath, request.getMetadata().getId());
        Files.createDirectories(basePath);

        writeRequestFile(request, basePath, entryList);

        String fileName = request.getMetadata().getId().replace("_SD_0000", "_SD_0001");
        String listName = writeEntryListFile(entryList, basePath, fileName,
                request.getMetadata().getMethod());

        request.getParameters().put("entryList", listName);
        writeJobControlFile(request, basePath, fileName);
        AEOResponse response = triggerAEO(request.getMetadata().getId());

        return Optional.ofNullable(response).map(r -> r.getStatus()).map(s -> s.get(0))
                .map(s -> s.getRequestStatus()).orElse(RequestStatus.failed_aeo_submit);
    }

    private void writeJobControlFile(Request request, Path path, String fileName)
            throws IOException {
        try {
            File f = new File(path.toString(), fileName + ".jcf");
            om.writeValue(f, request);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void writeRequestFile(Request request, Path path, EntryListInput entryList)
            throws IOException {
        try {
            File f = new File(path.toString(), request.getMetadata().getId() + ".req");
            Map<String, Object> requestMap = new HashMap<>();
            requestMap.put("metadata", request.getMetadata());
            requestMap.put("parameters", request.getParameters());
            requestMap.put("entryList", entryList);

            om.writeValue(f, requestMap);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String writeEntryListFile(EntryListInput entries, Path path, String fileName,
            String method) throws IOException {

        StringBuilder data = getFileHeader(method);
        addFileRows(data, entries, method);

        Path filePath = Paths.get(path.toString(), fileName + ".lst");
        Files.createFile(filePath);
        Files.write(filePath, data.toString().getBytes(), new OpenOption[] {});
        return filePath.getFileName().toString();
    }

    StringBuilder getFileHeader(String method) {
        StringBuilder data = new StringBuilder(
                "\"entry_id\",\"entry_number\",\"entry_name\",\"entry_type\",\"entry_role\",\"entry_class\",\"entry_status\",\"germplasm_id\""
                        + (needsNRepInEntries(method) ? ",\"nRep\"\n" : "\n"));
        return data;
    }

    void addFileRows(StringBuilder data, EntryListInput entries, String method) {
        boolean withNRep = RequestInputValidator.needsNRepInEntries(method);
        String template = "%s,%s,\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",%s"
                + (withNRep ? ",%s\n" : "\n");

        for (int i = 0; i < entries.getEntry_id().length; i++) {
            data.append(String.format(template, entries.getEntry_id()[i],
                    entries.getEntry_number()[i], entries.getEntry_name()[i],
                    entries.getEntry_type()[i], entries.getEntry_role()[i],
                    entries.getEntry_class()[i], entries.getEntry_status()[i],
                    entries.getGermplasm_id()[i], withNRep ? entries.getNrep()[i] : null));
        }
    }

    /**
     * Launches execution of AEO for the given request uuid
     * 
     * @param requestUuid
     * @return
     */
    public AEOResponse triggerAEO(String requestUuid) {
        log.info("trigger AEO for {}", requestUuid);
        AEOResponse response = aeoApiClient.post("randomize/" + requestUuid, null,
                AEOResponse.class);
        log.info("response from AEO: {}", response);
        return response;
    }

}