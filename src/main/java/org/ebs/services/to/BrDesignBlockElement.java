package org.ebs.services.to;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class BrDesignBlockElement {
    private String plotDbId;
    private long experimentId;
    private String occurrenceDbId;
    private int plotNum;
    private String designDbId;
    private String blockType;
    private String blockName;
    private String blockValue;
    private String blockLevelNumber;
    private long entryId;
    private long designX;
    private long designY;

    @Override
    public String toString() {
        return "BrDesignBlockElement[ plotDbId="+plotDbId+
        ", occurrenceDbId="+occurrenceDbId+
        ", designDbId="+designDbId+
        ", blockType="+blockType+
        ", blockValue="+blockValue+
        ", blockLevelNumber="+blockLevelNumber+"]";
    }
}