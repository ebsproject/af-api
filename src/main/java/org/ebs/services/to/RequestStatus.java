package org.ebs.services.to;

public enum RequestStatus {
    created,
    submitted,
    failed,
    queued,
    completed,
    failed_b4rapi_plots,
    failed_b4rapi_designArray,
    expired,
    failed_dpo,
    failed_aeo_submit,
    failed_aeo_status,
    processing_custom_design;
}