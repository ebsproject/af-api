package org.ebs.services.to;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter @ToString
public class AEOResponse {
    private List<AEOStatus> status;
}