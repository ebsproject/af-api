package org.ebs.services.to;

import java.util.Map;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonProperty;

import org.ebs.services.to.DesignDefinition.BOOL;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class DesignDefinitionParameter {
    @JsonProperty("variable_label")
    @NotBlank
    private String variableLabel;
    @NotBlank
    private String description;
    private BOOL visible;
    private Integer minimum;
    private Integer maximum;
    private String unit;
    private Map<String,String> values;
    @JsonProperty("Default")
    private String defaultValue;
    private BOOL disabled;
    private BOOL required;
    @JsonProperty("data_type")
    @NotBlank
    private String dataType;
    @JsonProperty("order_number")
    @Min(1)
    private int orderNumber;
    @JsonProperty("variable_abbrev")
    @NotBlank
    private String variableAbbrev;
    @JsonProperty("is_layout_variable")
    private BOOL isLayoutVariable;

    @Override
    public String toString() {
        return "DesignDefinitionParameter [dataType=" + dataType + ", defaultValue=" + defaultValue + ", description="
                + description + ", disabled=" + disabled + ", isLayoutVariable=" + isLayoutVariable + ", maximum="
                + maximum + ", minimum=" + minimum + ", orderNumber=" + orderNumber + ", required=" + required
                + ", unit=" + unit + ", values=" + values + ", variableAbbrev=" + variableAbbrev + ", variableLabel="
                + variableLabel + ", visible=" + visible + "]";
    }
}