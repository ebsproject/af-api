package org.ebs.services.to;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
@JsonInclude(value = Include.NON_EMPTY)
public class Request implements Serializable{
    public static final String CUSTON_DESIGN = "Custom Design";
    public static final String TRIAL_DESIGN = "Trial Design";
    private static final long serialVersionUID = 6741462145350804418L;
    
    private RequestMetadata metadata = new RequestMetadata();
    private Map<String,Object> parameters = new HashMap<>();
    private List<RequestEntry> entryList;
    @JsonIgnore
    private String createdBy;
    @JsonIgnore
    private RequestStatus status;

    public void addParameter(String key, Object value) {
        parameters.put(key, value);
    }

    @Override
    public String toString() {
        return "Request [metadata=" + metadata + ", num. parameters=" + parameters.size() + "]";
    }

}