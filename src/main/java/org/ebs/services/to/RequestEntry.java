package org.ebs.services.to;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter
@NoArgsConstructor @AllArgsConstructor
public class RequestEntry {
    
    private long id;
    private long entryId;
    private int entryNumber;
}