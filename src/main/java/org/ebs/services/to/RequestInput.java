package org.ebs.services.to;

import java.io.Serializable;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class RequestInput implements Serializable {
    private static final long serialVersionUID = -8541028545537524062L;

    @NotBlank
    private String category;
    @NotBlank
    private String type;
    @NotBlank
    private String engine;
    @NotBlank
    private String method;
    @NotBlank
    private String design;
    @NotBlank
    private String requestorId;
    @NotBlank
    private String organization_code;
    @NotBlank
    private String crop;
    @NotBlank
    private String program;
    @Min(1)
    private long experiment_id;
    @NotEmpty
    private List<Long> occurrence_id;
    @Valid
    @NotEmpty
    private List<ParameterInput> parameters;
    @Valid
    @NotNull
    private EntryListInput entryList;
}