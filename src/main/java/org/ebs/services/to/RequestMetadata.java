package org.ebs.services.to;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class RequestMetadata {
    private String id;
    private String timestamp;
    private String category;
    private String type;
    private String engine;
    private String method;
    private String design;
    private String requestorId;
    private String organization_code;
    private String crop;
    private String program;
    private long experiment_id;
    private List<Long> occurrence_id;

    @Override
    public String toString() {
        return "RequestMetadata [id=" + id + ", crop=" + crop + ", method=" + method + ", type=" + type + "]";
    }
    
}