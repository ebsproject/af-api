package org.ebs.services.to;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.ebs.util.FilterInput;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@RequiredArgsConstructor()
public class Property implements Serializable {
    private static final long serialVersionUID = 1769564437195490989L;

    private int id;
    private String code;
    private String name;
    private String variableLabel;
    private String description;
    private String type;
    private String dataType;
    /* These 3 attributes are extracted from property config */
    private Integer parentPropertyId;
    private int orderNumber;
    private boolean required;
    private boolean layoutVariable;
    private String statement;
    private List<FilterInput> filters = new ArrayList<>();

    // use of optional since all AfProperty in AfRequest are nullable
    public Property(Integer id) {
        this.id = Optional.ofNullable(id).orElse(0);
    }

    @Override
    public String toString() {
        return "Property [id=" + id + " code=" + code + ", parentPropertyId=" + parentPropertyId
                + ", required=" + required + ", type=" + type + ", filters: " + filters.size()
                + "]";
    }
}