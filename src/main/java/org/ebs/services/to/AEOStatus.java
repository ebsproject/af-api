package org.ebs.services.to;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class AEOStatus {
    private String message;
    private String messageType;
    private int messageCode;
    private RequestStatus requestStatus;

    @Override
    public String toString() {
        return String.format("AEOStatus [message=%s, messageCode=%s, requestStatus=%s]", message, messageCode, requestStatus);
    }
}