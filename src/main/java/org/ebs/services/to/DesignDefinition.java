package org.ebs.services.to;

import java.util.Map;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class DesignDefinition {
    
    public enum BOOL {
        
        T(true), F(false);

        boolean value;

        BOOL(boolean value) {
            this.value = value;
        }

        public boolean asBoolean() {
            return value;
        }
    }

    @NotNull @Valid
    private DesignDefinitionMetadata metadata;
    @NotNull @Valid
    private Map<String,Map<String,DesignDefinitionParameter>> parameters;

    @Override
    public String toString() {
        return "DesignDefinition [\n metadata=" + metadata + ",\n parameters=" + parameters + "]";
    }

   
}