package org.ebs.services.to;

import java.util.List;

import lombok.Data;
import lombok.ToString;

@Data @ToString
public class RequestFileDesignInput {
    private List<Integer> occurrenceIds;
    private List<Integer> entryIds;
    private String designFileName;
}
