package org.ebs.services.to;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class PropertyMeta {
    private String code;
    private String value;
 
    @Override
    public String toString() {
        return "PropertyMeta [code=" + code + ", value=" + value + "]";
    }
}