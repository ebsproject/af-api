package org.ebs.services.to;

import java.io.Serializable;

import javax.validation.constraints.NotEmpty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class EntryListInput implements Serializable {
    private static final long serialVersionUID = -8541028545537524062L;

    @NotEmpty
    private long[] entry_id;
    @NotEmpty
    private int[] entry_number;
    @NotEmpty
    private String[] entry_name;
    @NotEmpty
    private String[] entry_type;
    @NotEmpty
    private String[] entry_role;
    @NotEmpty
    private String[] entry_class;
    @NotEmpty
    private String[] entry_status;
    @NotEmpty
    private long[] germplasm_id;

    private int[] nrep;

}