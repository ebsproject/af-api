package org.ebs.services.to;

import java.util.List;
import java.util.Map;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter @Setter @RequiredArgsConstructor
public class DesignArray {

    private final Map<String, List<BrPlot>> plotList;
    private final Map<String,Integer> columnMap;
}