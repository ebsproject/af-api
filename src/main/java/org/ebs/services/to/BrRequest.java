package org.ebs.services.to;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class BrRequest {
	@JsonProperty("dbId")
    private int id;
    private String uuid;
	@JsonProperty("programDbId")
    private Integer cropProgramId;
    private String name;
	@JsonProperty("objectiveDbId")
    private Integer objectiveId;
	@JsonProperty("modelDbId")
    private Integer modelId;
    private String category;
    private RequestStatus status;
	@JsonProperty("errorDbId")
    private Integer errorId;
	@JsonProperty("occurrenceAnalysisPatternDbId")
    private Integer exptlocAnalysisPatternId;
	@JsonProperty("methodDbId")
    private Integer methodId;
	@JsonProperty("traitAnalysisPatternDbId")
    private Integer traitAnalysisPatternId;
    private Integer[] experimentDbIds;
    private Integer[] occurrenceDbIds;
    private Integer[] traitDbIds;
    private Integer[] predictionDbIds;
    private String ownerId;
}