package org.ebs.services.to;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
@JsonInclude(value = Include.NON_NULL)
public class BrPlot {

    private String plotDbId;
    private String occurrenceDbId;
    private long locationDbId;
    private String entryDbId;
    private String plotCode;
    private int plotNumber;
    private String plotType;
    private String rep;
    private String designX;
    private String designY;
    private int plotOrderNumber;
    private int paX;
    private int paY;
    private int fieldX;
    private int fieldY;
    private String plotStatus;
    private String plotQcCode;
    private String blockNumber;
    @JsonIgnore
    private int rowBlock;
    @JsonIgnore
    private int colBlock;

    @Override
    public String toString() {
        return "BrPlot [plotDbId="+plotDbId+
        ", occurrenceDbId="+occurrenceDbId+
        ", locationDbId="+locationDbId+
        ", entryDbId="+entryDbId+
        ", plotCode="+plotCode+
        ", plotNumber="+plotNumber+
        ", plotType="+plotType+
        ", rep="+rep+
        ", designX="+designX+
        ", designY="+designY+
        ", plotOrderNumber="+plotOrderNumber+
        ", plotStatus="+plotStatus+
        ", blockNumber="+blockNumber+
        ", rowBlock="+rowBlock+
        ", colBlock="+colBlock+"]";
    }

}