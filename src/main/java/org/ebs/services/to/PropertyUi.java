package org.ebs.services.to;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class PropertyUi implements Serializable{
    private static final long serialVersionUID = 1L;

    private Integer id;
    private Boolean visible;
    private Integer minimum;
    private Integer maximum;
    private String unit;
    private String defaultValue;
    private Boolean disabled;
    private Boolean multiple;
    private Boolean catalogue;

    @Override
    public String toString() {
        return "PropertyUi [id=" + id + "catalogue=" + catalogue + ", disabled=" + disabled + ", visible=" + visible
                + "]";
    }
}