package org.ebs.services.to;

import java.util.Arrays;

import javax.validation.constraints.NotBlank;

import org.hibernate.validator.constraints.Length;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ParameterInput {
    @NotBlank
    private String code;
    @Length(min = 1, max = 100)
    private String val;
    private String[] vals;

    @Override
    public String toString() {
        return "ParameterInput [code=" + code + val == null ?
            (", val=" + val) :
            (", vals=" + Arrays.toString(vals)) 
        + "]";
    }

}