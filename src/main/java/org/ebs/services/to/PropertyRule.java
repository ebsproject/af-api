package org.ebs.services.to;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class PropertyRule {
    private int id;
    private Integer propertyId;
    private String type;
    private String expression;
    private int group;
    private Integer orderNumber;
    private String notification;
    private String action;


    @Override
    public String toString() {
        return "PropertyRule [id=" + id + "expression=" + expression + ", group=" + group + ", orderNumber="
                + orderNumber + ", type=" + type + "]";
    }

}