package org.ebs.services.to;

import java.util.Date;

import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class DesignDefinitionMetadata {
    @JsonProperty("Rversion")
    @NotBlank
    private String rversion;
    private Date date;
    @NotBlank
    private String author;
    @NotNull @Email
    private String email;
    private String syntax;
    @NotBlank
    private String engine;
    @NotBlank
    private String method;
    @NotBlank
    private String design;
    @Min(1)
    private int modelVersion;
    @NotBlank
    @JsonProperty("organization_code")
    private String organizationCode;
    private String note;

    @Override
    public String toString() {
        return "DesignDefinitionMetadata [author=" + author + ", date=" + date + ", design=" + design + ", email="
                + email + ", engine=" + engine + ", method=" + method + ", modelVersion=" + modelVersion + ", note="
                + note + ", organizationCode=" + organizationCode + ", rversion=" + rversion + ", syntax=" + syntax
                + "]";
    }

}