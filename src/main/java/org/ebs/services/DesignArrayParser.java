package org.ebs.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import org.ebs.services.to.BrDesignBlockElement;
import org.ebs.services.to.BrPlot;
import org.ebs.services.to.DesignArray;
import org.ebs.services.to.Request;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
class DesignArrayParser {

    private static final Logger log = LoggerFactory.getLogger(DesignArrayParser.class);
    private static final String COL_OCCURRENCE = "occurrence",
        COL_ENTRY_ID = "entry_id",
        COL_FIELD_COL = "field_col",
        COL_FIELD_ROW = "field_row",
        COL_PLOT_NUMBER = "plot_number",
        COL_REPLICATE = "replicate",
        COL_BLOCK = "block",
        COL_COL_BLOCK = "colblock",
        COL_ROW_BLOCK = "rowblock";
    private static final String BLOCK_NAME_REP = "replicate",  //for irri is Replication
        BLOCK_NAME_BLOCK = "block", // for irri is Block
        BLOCK_NAME_ROW = "RowBlock",
        BLOCK_NAME_COL = "ColBlock",
        BLOCK_TYPE = "replication block";  // DB enforces this or 'treatment block'
    /**
     * Receives a String Row with comma separated values:
     * occurrence, plot_number, block, entry_id, field_row, field_col,
     * @param fileRow with design values
     * @return a brapi-formated plot element
     */
    DesignArray generateDesign(Scanner scanner, Request request) {
        Map<String,List<BrPlot>> plotList = new HashMap<>();

        Map<String,Integer> columnMap = getHeaderMap(scanner.nextLine());

        scanner.forEachRemaining(r ->{
            String[] row = elementsFromRow(r);

            int occNum = Integer.valueOf(row[columnMap.get(COL_OCCURRENCE)]);

            String occId = request.getMetadata().getOccurrence_id().get(occNum-1).toString();
            //long entryId = Long.valueOf(row[columnMap.get(COL_ENTRY_ID)]);

            BrPlot p = new BrPlot();
            if (columnMap.get(COL_FIELD_COL) != null && columnMap.get(COL_FIELD_ROW) != null) {
                p.setDesignX(row[columnMap.get(COL_FIELD_COL)]);
                p.setDesignY(row[columnMap.get(COL_FIELD_ROW)]);
            }
            p.setEntryDbId(extractEntryId(row, columnMap, request));
            p.setLocationDbId(0);
            p.setOccurrenceDbId(occId.toString());
            p.setPlotCode(row[columnMap.get(COL_PLOT_NUMBER)]);
            p.setPlotNumber(Integer.valueOf(row[columnMap.get(COL_PLOT_NUMBER)]));
            p.setPlotOrderNumber(Integer.valueOf(row[columnMap.get(COL_PLOT_NUMBER)]));
            p.setPlotStatus("active");
            p.setPlotType("plot");


            //only cimmyt rcbd doesn't have 'replicate' column
            if(columnMap.get(COL_REPLICATE) == null) {
                p.setRep(row[columnMap.get(COL_BLOCK)]);
                p.setBlockNumber(p.getRep());
            } else {
                p.setRep(row[columnMap.get(COL_REPLICATE)]);
                p.setBlockNumber(p.getRep());
                if(columnMap.get(COL_BLOCK) != null) {
                    p.setBlockNumber(row[columnMap.get(COL_BLOCK)]);
                }
            }
            if(columnMap.get(COL_ROW_BLOCK) != null && columnMap.get(COL_COL_BLOCK) != null) {
                p.setRowBlock(Integer.valueOf(row[columnMap.get(COL_ROW_BLOCK)]));
                p.setColBlock(Integer.valueOf(row[columnMap.get(COL_COL_BLOCK)]));
            }
            plotList.putIfAbsent(occId, new ArrayList<>());
            plotList.get(occId).add(p);
        });
        return new DesignArray(plotList, columnMap);
    }

    String extractEntryId(String[] row, Map<String,Integer> columnMap, Request request) {
        String entry = row[columnMap.get(COL_ENTRY_ID)];
        if(request.getMetadata().getType().equals(Request.CUSTON_DESIGN)) {
            return String.valueOf(request.getEntryList().stream()
                .filter(e -> e.getEntryNumber() == Integer.parseInt(entry))
                .findFirst()
                .orElseThrow()
                .getEntryId());
        }
        return entry;
    }

    /**
     * Generates a map where the key is the header name and the value its position within the header row starting from 0
     * @param headerRow
     * @return
     */
    Map<String,Integer> getHeaderMap(String headerRow) {
        Map<String,Integer> map = new HashMap<>();
        Arrays.asList(elementsFromRow(headerRow))
            .forEach( s -> map.put(s,map.size()));
        log.trace("headerMap>>>> {}", map);
        return map;
    }

    String[] elementsFromRow(String row) {
        return row.replaceAll("\"", "")
        .replaceAll("\\t+", ",")
        .replaceAll(" +", ",")
        .split(",");
    }

    /**
     * generates a Design Table for a request
     * @param designArray with a plot list after parsing result files
     * @param plotResult with ids of the generated plots
     * @param request which originated the design, if extra metadata is required
     * @return a design table to be sent to B4R
     */
    List<BrDesignBlockElement> parseDesignTable(DesignArray designArray, Map<String, List<BrPlot>> plotResult, Request request) {
        Map<String,Integer> columns = designArray.getColumnMap();

        List<BrDesignBlockElement> designTable = new ArrayList<>()
            ,designBlock = new ArrayList<>();

        plotResult.forEach((occId,plots) -> {

            List<BrPlot> arrayPlots = designArray.getPlotList().get(occId);
            
            long numReps = arrayPlots.stream()
            .mapToInt(p -> Integer.parseInt(p.getRep()))
            .distinct().count();
            int numRowBlocks = arrayPlots.size();
            int i = 0;

            for(BrPlot plot : plots){
                BrPlot arrayPlot = arrayPlots.get(i++);

                BrDesignBlockElement rep = createReplicateBlock(occId,
                    plot.getPlotDbId(),
                    arrayPlot.getRep(),
                    arrayPlot.getRep(),
                    BLOCK_NAME_REP);

                designTable.add(rep);

                //when it has block level
                if(columns.get(COL_REPLICATE) != null && columns.get(COL_BLOCK) != null) {
                    designBlock.add(createReplicateBlock(occId,
                        rep.getPlotDbId(),
                        Integer.valueOf(arrayPlot.getBlockNumber()),
                        Integer.valueOf(arrayPlot.getBlockNumber()) + (int)numReps,  //  block-design-id continues after rep-design-id
                        BLOCK_NAME_BLOCK));
                }
                //  only row column design
                if(columns.get(COL_ROW_BLOCK) != null && columns.get(COL_COL_BLOCK) != null) {

                    designBlock.add(createReplicateBlock(occId,
                        rep.getPlotDbId(),
                        arrayPlot.getRowBlock(),
                        arrayPlot.getRowBlock() + (int)numReps, //  row-block-design-id continues after rep-design-id
                        BLOCK_NAME_ROW));

                    designBlock.add( createReplicateBlock(occId,
                        rep.getPlotDbId(),
                        arrayPlot.getColBlock(),
                        arrayPlot.getColBlock() + (int)numReps + numRowBlocks, //  col-block-design-id continues after row-block-design-id
                        BLOCK_NAME_COL));

                }
            }
        });

        designTable.addAll(designBlock);
        return designTable;
    }

    private BrDesignBlockElement createReplicateBlock(String occId, String plotId, int blockValue, int designId, String blockName) {
        return createReplicateBlock(occId, plotId, String.valueOf(blockValue), String.valueOf(designId), blockName);
    }

    private BrDesignBlockElement createReplicateBlock(String occId, String plotId, String blockValue, String designId, String blockName) {
        BrDesignBlockElement d = new BrDesignBlockElement();
        d.setBlockType(BLOCK_TYPE);
        d.setOccurrenceDbId(String.valueOf(occId));
        d.setPlotDbId(plotId);
        d.setBlockValue(blockValue);
        d.setDesignDbId(designId);
        d.setBlockName(blockName);
        //replicate blocks are always level 1, other (design) blocks are level 2
        d.setBlockLevelNumber(blockName.equals(BLOCK_NAME_REP) ? "1" : "2");

        return d;
    }
}