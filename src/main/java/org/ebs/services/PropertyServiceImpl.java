package org.ebs.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.ebs.model.AfProperty;
import org.ebs.model.AfPropertyConfig;
import org.ebs.model.repos.AfPropertyConfigRepository;
import org.ebs.model.repos.AfPropertyMetaRepository;
import org.ebs.model.repos.AfPropertyRepository;
import org.ebs.services.to.Property;
import org.ebs.services.to.PropertyMeta;
import org.ebs.services.to.PropertyRule;
import org.ebs.services.to.PropertyUi;
import org.ebs.util.FilterInput;
import org.ebs.util.FilterMod;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class PropertyServiceImpl implements PropertyService {

    private final AfPropertyRepository propertyRepository;
    private final AfPropertyConfigRepository propertyConfigRepository;
    private final ConversionService conversionService;
    private final AfPropertyMetaRepository afPropertyMetaRepository;

    @Override
    public Optional<Property> findProperty(int propertyId, int parentPropertyId) {
        return propertyConfigRepository
                .findByPropertyIdAndConfigPropertyIdAndDeletedIsFalse(parentPropertyId, propertyId)
                .map(pc -> conversionService.convert(pc, Property.class));
    }

    @Override
    public Optional<Property> findCatalog(int catalogPropertyId, List<FilterInput> filters) {

        Optional<Property> result = propertyConfigRepository
                .findByPropertyIdAndConfigPropertyTypeAndDeletedIsFalse(catalogPropertyId,
                        "catalog_root")
                .stream().findFirst().map(pc -> conversionService.convert(pc, Property.class));

        result.ifPresent((p) -> p.setFilters(filters == null ? new ArrayList<>() : filters));

        return result;
    }

    @Override
    public List<Property> findCatalogElements(Property catalogProperty) {
        int catalogPropertyId = catalogProperty.getId();
        Optional<AfProperty> prop = propertyRepository.findByIdAndDeletedIsFalse(catalogPropertyId);
        if (!prop.isPresent())
            return null;

        List<AfPropertyConfig> configs;
        if (prop.get().getType().equals("catalog_root")) {
            catalogProperty.getFilters()
                    .add(new FilterInput("configProperty.type", "catalog_item", FilterMod.EQ));
        }
        catalogProperty.getFilters()
                .add(new FilterInput("property.id", "" + catalogPropertyId, FilterMod.EQ));

        configs = propertyConfigRepository.findByCriteria(AfPropertyConfig.class,
                catalogProperty.getFilters(), (SortInput) null, (PageInput) null).getContent();

        return configs.stream().map(pc -> conversionService.convert(pc, Property.class))
                .collect(Collectors.toList());
    }

    @Override
    public Optional<PropertyUi> findPropertyUi(int propertyId, int configPropertyId) {
        return propertyConfigRepository
                .findByPropertyIdAndConfigPropertyIdAndDeletedIsFalse(propertyId, configPropertyId)
                .map(pc -> conversionService.convert(pc.getUi(), PropertyUi.class));

    }

    @Override
    public Set<PropertyRule> findPropertyRules(int propertyId, int configPropertyId) {
        Optional<AfPropertyConfig> afpropConf = propertyConfigRepository
                .findByPropertyIdAndConfigPropertyIdAndDeletedIsFalse(propertyId, configPropertyId);

        return afpropConf.orElse(null).getRules().stream().filter(r -> !r.getDeleted())
                .map(r -> conversionService.convert(r, PropertyRule.class))
                .collect(Collectors.toSet());
    }

    @Override
    public Optional<Property> findPropertyByCode(String propertyCode, int parentPropertyId) {
        return propertyConfigRepository
                .findByPropertyIdAndConfigPropertyCodeAndDeletedIsFalse(parentPropertyId,
                        propertyCode)
                .map(pc -> conversionService.convert(pc, Property.class));
    }

    @Override
    public Set<PropertyMeta> findPropertyMeta(int propertyId) {
        return afPropertyMetaRepository.findByPropertyIdAndDeletedIsFalse(propertyId).stream()
                .map(r -> conversionService.convert(r, PropertyMeta.class))
                .collect(Collectors.toSet());
    }

}