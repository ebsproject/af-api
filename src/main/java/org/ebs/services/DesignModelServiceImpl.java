package org.ebs.services;

import static org.ebs.model.CatalogProperty.CATALOG.DESIGN_MODEL;

import java.util.Optional;

import org.ebs.model.AfProperty;
import org.ebs.model.AfPropertyConfig;
import org.ebs.model.repos.AfPropertyConfigRepository;
import org.ebs.model.repos.AfPropertyRepository;
import org.ebs.model.repos.AfPropertyUiRepository;
import org.ebs.services.to.DesignDefinition;
import org.ebs.services.to.DesignDefinitionParameter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor(onConstructor = @__({@Autowired}))
class DesignModelServiceImpl implements DesignModelService {

    private final AfPropertyRepository propertyRepository;
    private final AfPropertyConfigRepository propertyConfigRepository;
    private final AfPropertyUiRepository propertyUiRepository;
    private final ConversionService converter;

    @Override
    @Transactional(readOnly = false)
    public int createDesignDefinition(DesignDefinition designDefinition) {

        AfProperty newDesign = addNewDesignIntoCatalog(designDefinition);

        designDefinition.getParameters().get("input").forEach((k,v) -> {
            associatePropertyToDesign(newDesign, getOrCreateProperty(k, v), v);
        });

        return newDesign.getId();
    }

    /**
     * Adds a new element to the catalog specified. This means adding it as a new {@link AfProperty}
     * and registering the relationship with the catalog as a new {@link AfPropertyConfig}
     * @param designDefinition to be added as new item of a catalog
     * @param catalogProp representing the catalog where the item is added
     * @return the {@link AfProperty} representing the item added. 
     */

    AfProperty addNewDesignIntoCatalog(DesignDefinition designDefinition) {
        AfProperty catalog = propertyRepository.findById(DESIGN_MODEL.id())
            .orElseThrow(() -> new RuntimeException("design model catalog not found"));
        
        AfProperty designProp = converter.convert(designDefinition, AfProperty.class);
        designProp = propertyRepository.save(designProp);

        int maxOrderNumber = propertyConfigRepository.findMaxOrderNumberByPropertyIdAndDeletedIsFalse(catalog.getId());

        AfPropertyConfig catalogItemConfig = new AfPropertyConfig();
        catalogItemConfig.setOrderNumber(maxOrderNumber + 1);
        catalogItemConfig.setProperty(catalog);
        catalogItemConfig.setConfigProperty(designProp);
        propertyConfigRepository.save(catalogItemConfig);

        return designProp;
    }

    void associatePropertyToDesign(AfProperty design, AfProperty designProp, DesignDefinitionParameter designParam) {
        AfPropertyConfig propConf = converter.convert(designParam, AfPropertyConfig.class);

        propConf.setUi(propertyUiRepository.save(propConf.getUi()));
        propConf.setProperty(design);
        propConf.setConfigProperty(designProp);
        propertyConfigRepository.save(propConf);
    }

    AfProperty getOrCreateProperty(String code, DesignDefinitionParameter parameter) {
        Optional<AfProperty> p = propertyRepository.findByCodeAndDeletedIsFalse(code);
        return p.orElseGet(() -> {
            AfProperty newProp = converter.convert(parameter, AfProperty.class);
            newProp.setCode(code);
            return propertyRepository.save(newProp);
        });
    }

}