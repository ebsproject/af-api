package org.ebs.services;

import java.io.IOException;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.ebs.model.AfRequest;
import org.ebs.model.repos.AfRequestRepository;
import org.ebs.services.to.Request;
import org.ebs.services.to.RequestEntry;
import org.ebs.services.to.RequestFileDesignInput;
import org.ebs.services.to.RequestInput;
import org.ebs.services.to.RequestStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor(onConstructor = @__({ @Autowired }))
class RequestServiceImpl implements RequestService {

    private final AfRequestRepository afRequestRepository;
    private final ConversionService conversionService;
    private final DPOService dpo;

    @Override
    @Transactional(readOnly = false)
    public Request createRequest(RequestInput requestInput) {

        AfRequest request = conversionService.convert(requestInput, AfRequest.class);
        request.setUuid(UUID.randomUUID().toString());
        request.setStatus(RequestStatus.created);
        request = afRequestRepository.save(request);

        Request req = conversionService.convert(request, Request.class);

        try {
            RequestStatus status = dpo.processDesignRequest(req, requestInput.getEntryList());
            request.setStatus(status);
        } catch (IOException e) {
            request.setStatus(RequestStatus.failed_dpo);
            e.printStackTrace();
        }
        afRequestRepository.save(request);
        req.setStatus(request.getStatus());
        return req;
    }

    @Override
    public List<Request> findByStatus(RequestStatus status) {
        return afRequestRepository.findByStatusAndDeletedIsFalse(status).stream()
            .map(r -> conversionService.convert(r, Request.class))
            .collect(Collectors.toList());

    }

    @Override
    public Request findByUuid(String uuid) {
        AfRequest afReq = afRequestRepository.findByUuid(uuid.replace("_SD_0000",""));
        Request req = conversionService.convert(afReq, Request.class);
        req.setEntryList(afReq.getEntries().stream()
            .map(e -> conversionService.convert(e, RequestEntry.class))
            .collect(Collectors.toList()));
        return req;
    }

    @Override
    @Transactional(readOnly = false)
    public RequestStatus updateStatus(String uuid, RequestStatus status) {
        AfRequest afReq = afRequestRepository.findByUuid(uuid.replace("_SD_0000",""));
        afReq.setStatus(status);
        afReq = afRequestRepository.save(afReq);
        return afReq.getStatus();
    }

    @Override
    @Transactional(readOnly = false)
    public Request createFileDesignRequest(RequestFileDesignInput requestInput) {
        AfRequest request = conversionService.convert(requestInput, AfRequest.class);
        request.setUuid(UUID.randomUUID().toString());
        request.setStatus(RequestStatus.processing_custom_design);
        request = afRequestRepository.save(request);
  
        return conversionService.convert(request, Request.class);
    }
}
