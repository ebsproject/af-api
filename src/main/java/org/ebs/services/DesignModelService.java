package org.ebs.services;

import org.ebs.services.to.DesignDefinition;

public interface DesignModelService {
    int createDesignDefinition(DesignDefinition designDefinition);
}