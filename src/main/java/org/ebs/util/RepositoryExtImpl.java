package org.ebs.util;

import static java.util.Collections.singletonList;
import static java.util.Optional.ofNullable;
import static java.util.stream.Collectors.toList;
import static javax.persistence.criteria.JoinType.INNER;
import static javax.persistence.criteria.JoinType.LEFT;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

class RepositoryExtImpl<T> implements RepositoryExt<T> {

    private final int minPageNumber = 0;
    private final int maxPageSize = 500;
    private Pageable defaultPage = PageRequest.of(minPageNumber, maxPageSize);
    private final String dotExpr = "\\.";
    private DateCoercing dateCoercing = new DateCoercing();

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Connection<T> findByCriteria(Class<T> entityClass, List<FilterInput> filters,
            SortInput sort, PageInput pageInput) {
        return doFindByCriteria(entityClass, filters, singletonList(sort), pageInput, false);
    }

    @Override
    public Connection<T> findByCriteria(Class<T> entityClass, List<FilterInput> filters,
            SortInput sort, PageInput pageInput, boolean disjuntionFilters) {
        return doFindByCriteria(entityClass, filters, singletonList(sort), pageInput,
                disjuntionFilters);
    }

    @Override
    public Connection<T> findByCriteria(Class<T> entityClass, List<FilterInput> filters,
            List<SortInput> sort, PageInput pageInput, boolean disjuntionFilters) {
        return doFindByCriteria(entityClass, filters, sort, pageInput, disjuntionFilters);
    }

    Connection<T> doFindByCriteria(Class<T> entityClass, List<FilterInput> filters,
            List<SortInput> sort, PageInput pageInput, boolean disjuntionFilters) {

        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<T> query = builder.createQuery(entityClass);
        Root<T> queryRoot = query.from(entityClass);

        Predicate predicates = createWherePredicate(builder, queryRoot, filters, disjuntionFilters);

        query.select(queryRoot);
        query.where(predicates);
        createSort(builder, query, queryRoot, sort);

        Pageable page = createPage(pageInput);

        List<T> resultList = createPagedQuery(entityManager, query, page).getResultList();

        long totalCount = countForQuery(builder, entityClass, filters);

        return new Connection<T>(resultList, page, totalCount);

    }

    private TypedQuery<T> createPagedQuery(EntityManager em, CriteriaQuery<T> criteria,
            Pageable page) {
        return entityManager.createQuery(criteria).setFirstResult((int) (page.getOffset()))
                .setMaxResults(page.getPageSize());
    }

    private Pageable createPage(PageInput pageInput) {
        return pageInput == null ? defaultPage
                : PageRequest.of(Math.max(minPageNumber, pageInput.getNumber() - 1),
                        Math.min(maxPageSize, pageInput.getSize()));
    }

    private Predicate createWherePredicate(CriteriaBuilder builder, Root<T> queryRoot,
            List<FilterInput> filters, boolean disjuntionFilters) {
        List<Predicate> predicateList = new ArrayList<>();

        ofNullable(filters).ifPresent(fs -> {

            fs.forEach(f -> {
                Path<String> filterPath = createFilterPath(f, queryRoot);
                switch (f.getMod()) {
                case LK:
                    predicateList.add(builder.like(filterPath, "%" + f.getVal() + "%"));
                    break;
                case NULL:
                    predicateList.add(builder.isNull(filterPath));
                    break;
                case NOTNULL:
                    predicateList.add(builder.isNotNull(filterPath));
                    break;
                case EQ:
                default: // default is equals
                    predicateList.add(builder.equal(filterPath, typedValueOf(f, filterPath)));
                    break;
                }
            });
        });

        Predicate[] predicates = predicateList.toArray(new Predicate[0]);
        Predicate allFiltersPredicate = disjuntionFilters ? builder.or(predicates)
                : builder.and(predicates),
                notDeletedPredicate = builder.equal(queryRoot.get("deleted"), false);

        return builder.and(allFiltersPredicate, notDeletedPredicate);
    }

    private Path<String> createFilterPath(FilterInput f, Root<T> root) {
        String[] filterPaths = f.getCol().split(dotExpr);
        Path<String> path = root.get(filterPaths[0]);
        Class<?> clazz = path.getJavaType();
        switch (filterPaths.length) {
        case 2:
            if (clazz == List.class || clazz == Set.class) {
                path = root.join(filterPaths[0], INNER).get(filterPaths[1]);
            } else {
                root.join(filterPaths[0], LEFT);
                path = path.get(filterPaths[1]);
            }
            break;
        case 3:
            if (clazz == List.class || clazz == Set.class) {
                clazz = root.join(filterPaths[0], INNER).get(filterPaths[1]).getJavaType();
                if (clazz == List.class || clazz == Set.class) {
                    path = root.join(filterPaths[0], INNER).join(filterPaths[1], INNER)
                            .get(filterPaths[2]);
                } else {
                    path = root.join(filterPaths[0], INNER).join(filterPaths[1], LEFT)
                            .get(filterPaths[2]);
                }
            } else {
                path = root.join(filterPaths[0], LEFT).join(filterPaths[1], LEFT)
                        .get(filterPaths[2]);
            }
            break;
        }
        return path;
    }

    private Object typedValueOf(FilterInput filter, Path<String> filterPath) {
        Class<?> clazz = filterPath.getJavaType();

        if (clazz == Boolean.class) {
            return Boolean.parseBoolean(filter.getVal());
        } else if (clazz == UUID.class) {
            return UUID.fromString(filter.getVal());
        } else if (clazz == Date.class) {
            return dateCoercing.parseValue(filter.getVal());
        }

        return filter.getVal();

    }

    private void createSort(CriteriaBuilder builder, CriteriaQuery<T> criteria, Root<T> queryRoot,
            List<SortInput> sort) {
        if (sort == null || sort.get(0) == null)
            return;

        criteria.orderBy(
                sort.stream().map(s -> createSingleOrder(builder, queryRoot, s)).collect(toList()));
    }

    private Order createSingleOrder(CriteriaBuilder builder, Root<T> queryRoot,
            SortInput sortInput) {
        Expression<T> sortExpr = queryRoot.get(sortInput.getCol());
        return sortInput.getMod().equals(SortMod.DES) ? builder.desc(sortExpr)
                : builder.asc(sortExpr);
    }

    private long countForQuery(CriteriaBuilder builder, Class<T> entityClass,
            List<FilterInput> filters) {
        CriteriaQuery<Long> queryCount = builder.createQuery(Long.class);

        Root<T> queryRoot = queryCount.from(entityClass);

        Predicate predicates = createWherePredicate(builder, queryRoot, filters, false);
        queryCount.select(builder.count(builder.literal(1))).where(predicates);

        long totalCount = entityManager.createQuery(queryCount).getSingleResult();

        return totalCount;

    }

    @Override
    public <E> int removeById(Class<E> entityClass, Integer id) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaDelete<E> delete = builder.createCriteriaDelete(entityClass);
        Root<E> queryRoot = delete.from(entityClass);
        delete.where(builder.equal(queryRoot.get("id"), id));

        return entityManager.createQuery(delete).executeUpdate();
    }

    @Override
    public Connection<T> findByCriteria(Class<T> entityClass, List<FilterInput> filters,
            SortInput sort, Pageable page) {
        return findByCriteria(entityClass, filters, sort,
                new PageInput(page.getPageNumber(), page.getPageSize()));
    }

    @Override
    public Connection<T> findByCriteria(Class<T> entityClass, List<FilterInput> filters,
            SortInput sort, Pageable page, boolean disjuntionFilters) {
        return findByCriteria(entityClass, filters, sort,
                new PageInput(page.getPageNumber(), page.getPageSize()), disjuntionFilters);
    }

    @Override
    public Page<T> findByCriteria(Class<T> entityClass, List<FilterInput> filters,
            List<SortInput> sort, Pageable page, boolean disjuntionFilters) {
        return findByCriteria(entityClass, filters, sort,
                new PageInput(page.getPageNumber(), page.getPageSize()), disjuntionFilters);
    }
}