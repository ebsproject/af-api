package org.ebs.util;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PageInput implements Serializable {
    private static final long serialVersionUID = 2237035358831275209L;

    private int number;
    private int size;

}