package org.ebs;

import org.ebs.services.FPOService;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import lombok.RequiredArgsConstructor;

@Configuration
@EnableScheduling
@RequiredArgsConstructor
public class SchedulingConfig {
	
	private static final int fixedDelay = 1000;
	private final FPOService fpoService;

	@Scheduled(fixedDelay = fixedDelay)
	public void callDesignFpo() {
		fpoService.proccessDesignArray();
	}

	@Scheduled(fixedDelay = fixedDelay)
	public void callCustomDesignFpo() {
		fpoService.proccessCustomDesignArray();
	}

}
