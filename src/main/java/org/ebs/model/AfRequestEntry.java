package org.ebs.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.ebs.util.Auditable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "request_entry", schema = "af")
@Getter @Setter
@NoArgsConstructor @AllArgsConstructor
public class AfRequestEntry extends Auditable{

    private static final long serialVersionUID = -6660283890352656547L;

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column
    private long id;
    @Column(name = "experiment_id")
    private long experimentId;
    @Column(name = "entry_id")
    private long entryId;
    @ManyToOne
    @JoinColumn(name = "request_id")
    private AfRequest request;
    @Column(name = "entry_number")
    private int entryNumber;

    @Override
    public String toString() {
        return "AfRequestEntry [id=" + id + ", entryId=" + entryId + ", request.id=" + request.getId() + "]";
    }

}
