package org.ebs.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.ebs.util.Auditable;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "property_config", schema = "af")
@Getter @Setter
public class AfPropertyConfig extends Auditable{

    private static final long serialVersionUID = -8509352166890150754L;
    
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column
    private int id;
    @ManyToOne
    @JoinColumn(name = "property_id")
    private AfProperty property;
    @ManyToOne
    @JoinColumn(name = "config_property_id")
    private AfProperty configProperty;
    @ManyToOne
    @JoinColumn(name = "property_ui_id")
    private AfPropertyUi ui;
    @Column(name = "is_required")
    private boolean required;
    @Column(name = "order_number")
    private int orderNumber;
    @OneToMany(mappedBy = "propertyConfig")
    private Set<AfPropertyRule> rules;
    @Column(name = "is_layout_variable")
    private boolean layoutVariable;

    @Override
    public String toString() {
        return "AfPropertyConfig [id=" + id + ", orderNumber=" + orderNumber + ", required=" + required + "]";
    }
}
