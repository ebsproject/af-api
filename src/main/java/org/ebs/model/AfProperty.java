package org.ebs.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.ebs.util.Auditable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "property", schema = "af")
@Getter
@Setter()
@NoArgsConstructor
public class AfProperty extends Auditable {

    private static final long serialVersionUID = 1653962747514574667L;

    public AfProperty(int id) {
        this.id = id;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private int id;
    @Column
    private String code;
    @Column
    private String name;
    @Column(name = "label")
    private String variableLabel;
    @Column
    private String description;
    @Column
    private String type;
    @Column(name = "data_type")
    private String dataType;
    @OneToMany(mappedBy = "property", cascade = CascadeType.ALL)
    private Set<AfPropertyMeta> meta;
    @Column
    private String statement;

    @Override
    public String toString() {
        return "AfProperty [ id=" + id + ", code=" + code + ", name=" + name + ", type=" + type
                + "]";
    }
}