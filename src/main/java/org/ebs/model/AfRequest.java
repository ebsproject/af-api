package org.ebs.model;

import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.ebs.services.to.RequestStatus;
import org.ebs.util.Auditable;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "request", schema = "af")
@Getter @Setter
public class AfRequest extends Auditable{

    private static final long serialVersionUID = -6660283890352656547L;

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column
    private int id;
    //TODO convert to UUID type, also in DB
    @Column
    private String uuid;
    @Column
    private String category;
    @Column
    private String type;
    @ManyToOne
    @JoinColumn(name = "method_id")
    private AfProperty method;
    @Column
    private String design;
    @Column(name = "requestor_id")
    private String requestorId;
    @Column
    private String institute;
    @Column
    private String crop;
    @Column
    private String program;
    @Column
    @Enumerated(EnumType.STRING)
    private RequestStatus status;
    @OneToMany(mappedBy = "request", cascade = CascadeType.ALL)
    private List<AfRequestParameter> parameters;
    @OneToMany(mappedBy = "request", cascade = CascadeType.ALL)
    private List<AfRequestEntry> entries;
    @Column
    private String engine;

    @Override
    public String toString() {
        return "AfRequest [id=" + id + "category=" + category + ", crop=" + crop + ", method=" + method + ", type="
                + type + ", status="+status+"]";
    }
}
