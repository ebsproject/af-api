package org.ebs.model.repos;

import java.util.Optional;

import org.ebs.model.AfProperty;
import org.ebs.util.RepositoryExt;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AfPropertyRepository extends JpaRepository<AfProperty,Integer>,
        RepositoryExt<AfProperty>{

        Optional<AfProperty> findByCodeAndDeletedIsFalse(String code);
        Optional<AfProperty> findByIdAndDeletedIsFalse(Integer id);
}