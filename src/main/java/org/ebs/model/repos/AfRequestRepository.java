package org.ebs.model.repos;

import java.util.List;

import org.ebs.model.AfRequest;
import org.ebs.services.to.RequestStatus;
import org.ebs.util.RepositoryExt;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AfRequestRepository extends JpaRepository<AfRequest,Integer>,
        RepositoryExt<AfRequest>{

    List<AfRequest> findByStatusAndDeletedIsFalse(RequestStatus status);
    AfRequest findByUuid(String uuid);
}