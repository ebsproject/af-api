package org.ebs.model.repos;

import java.util.Optional;
import java.util.Set;

import org.ebs.model.AfPropertyConfig;
import org.ebs.util.RepositoryExt;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface AfPropertyConfigRepository extends JpaRepository<AfPropertyConfig,Integer>,
        RepositoryExt<AfPropertyConfig>{

    Set<AfPropertyConfig> findByPropertyIdAndConfigPropertyTypeAndDeletedIsFalse(int propertyId, String type);
    Optional<AfPropertyConfig> findByPropertyIdAndConfigPropertyIdAndDeletedIsFalse(int propertyId, int configPropertyId);
    Optional<AfPropertyConfig> findByPropertyIdAndConfigPropertyCodeAndDeletedIsFalse(int propertyId, String configPropertyCode);
    Set<AfPropertyConfig> findByPropertyIdAndDeletedIsFalse(int propertyId);

    @Query("select max(orderNumber) from AfPropertyConfig where property.id = :propertyId")
    int findMaxOrderNumberByPropertyIdAndDeletedIsFalse(int propertyId);
}