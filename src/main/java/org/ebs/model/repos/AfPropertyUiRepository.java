package org.ebs.model.repos;

import org.ebs.model.AfPropertyUi;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AfPropertyUiRepository extends JpaRepository<AfPropertyUi,Integer>{

}