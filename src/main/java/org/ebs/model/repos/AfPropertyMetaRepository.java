package org.ebs.model.repos;

import java.util.Set;

import org.ebs.model.AfPropertyMeta;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AfPropertyMetaRepository extends JpaRepository<AfPropertyMeta,Integer>{

    Set<AfPropertyMeta> findByPropertyIdAndDeletedIsFalse(int propertyId);
}