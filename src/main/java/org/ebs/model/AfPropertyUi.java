package org.ebs.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.ebs.util.Auditable;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "property_ui", schema = "af")
@Getter @Setter
public class AfPropertyUi extends Auditable {

    private static final long serialVersionUID = -8738838409003236783L;

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column
    private int id;
    @Column(name= "is_visible")
    private boolean visible = true;
    @Column
    private Integer minimum;
    @Column
    private Integer maximum;
    @Column
    private String unit;
    @Column(name= "\"default\"")
    private String defaultValue;
    @Column(name= "is_disabled")
    private boolean disabled = false;
    @Column(name= "is_multiple")
    private boolean multiple = false;
    @Column(name= "is_catalogue")
    private boolean catalogue = false;

    @Override
    public String toString() {
        return "AfPropertyUi [id=" + id + "catalogue=" + catalogue +  ", multiple=" + multiple + ", visible=" + visible
                + "]";
    }
}