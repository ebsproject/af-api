package org.ebs.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.ebs.util.Auditable;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "property_rule", schema = "af")
@Getter @Setter
public class AfPropertyRule extends Auditable{

    private static final long serialVersionUID = 1300078745187248616L;

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column
    private int id;
    @Column(name = "property_id")
    private Integer propertyId;
    @Column
    private String type;
    @Column
    private String expression;
    @Column
    private int group;
    @ManyToOne
    @JoinColumn(name = "property_config_id")
    private AfPropertyConfig propertyConfig;
    @Column(name="order_number")
    private Integer orderNumber;
    @Column
    private String notification;
    @Column
    private String action;
    @Override
    public String toString() {
        return "AfPropertyRule [id=" + id + "expression=" + expression + ", propertyId=" + propertyId + ", type="
                + type + "]";
    }
}