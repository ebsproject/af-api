package org.ebs.model;

public final class CatalogProperty {

    public enum CATALOG {
        OBJECTIVE(1), TRAIT_PATTERN(2), EXPT_PATTERN(3), DESIGN_MODEL(4), PREDICTION(5), ERROR(
                6), EXPT_ID(130), OCC_ID(131), ANALYSIS_CONFIG(134), ASRMEL_OPTIONS(137), TABULATE(
                        138), FORMULA(139), RESIDUAL(140), ANALYSIS_MODULE_FIELDS(159);

        private int id;

        CATALOG(int i) {
            id = i;
        }

        public int id() {
            return id;
        }

    }

    public static final String randPREPirri = "randPREPirri";
    public static final String randAugUnrep = "randAugUnrep";
}