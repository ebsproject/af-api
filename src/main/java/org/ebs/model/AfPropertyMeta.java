package org.ebs.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.ebs.util.Auditable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "property_meta", schema = "af")
@Getter @Setter @NoArgsConstructor
public class AfPropertyMeta extends Auditable {

    private static final long serialVersionUID = 1653962747514574667L;

    public AfPropertyMeta(String code, String value) {
        this.code = code;
        this.value = value;
    }

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column
    private int id;
    @Column
    private String code;
    @Column
    private String value;
    @ManyToOne
    @JoinColumn(name = "property_id")
    private AfProperty property;

    @Override
    public String toString() {
        return "AfPropertyMeta [id=" + id + ", code=" + code + ", value=" + value + "]";
    }    
}