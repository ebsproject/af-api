package org.ebs.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.ebs.util.Auditable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "request_parameter", schema = "af")
@Getter @Setter @NoArgsConstructor
public class AfRequestParameter extends Auditable{

    private static final long serialVersionUID = -2422920019043079799L;

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column
    private int id;
    @ManyToOne
    @JoinColumn(name = "request_id", nullable = false, updatable = false)
    private AfRequest request;
    @ManyToOne
    @JoinColumn(name = "property_id", nullable = false)
    private AfProperty property;
    @Column
    private String value;

    public AfRequestParameter(AfProperty property, String value) {
        this.property = property;
        this.value = value;
    }

    @Override
    public String toString() {
        return "AfRequestParameter [id=" + id + ", value=" + value + "]";
    }

}