package org.ebs.rest.validation;

import java.time.Instant;
import java.util.Date;
import java.util.Map;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.ebs.services.to.DesignDefinition;
import org.ebs.services.to.DesignDefinitionMetadata;
import org.ebs.services.to.DesignDefinitionParameter;
import org.springframework.beans.factory.annotation.Autowired;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor(onConstructor = @__({ @Autowired }))
public class DesignDefinitionValidator implements ConstraintValidator<ValidDesignDefinition,DesignDefinition> {

    private boolean valid = true;

    @Override
    public boolean isValid(DesignDefinition designDefinition, ConstraintValidatorContext ctx) {
        valid = true;
        checkMetadata(designDefinition.getMetadata());
        return validateInputs(designDefinition.getParameters().get("input"), ctx);
    }

    private void checkMetadata(DesignDefinitionMetadata metadata) {
        if( metadata.getDate() == null) {
            metadata.setDate(Date.from(Instant.now()));
        }
    }
    
    private boolean validateInputs(Map<String, DesignDefinitionParameter> inputs, ConstraintValidatorContext ctx) {
        String validDataTypes = "integer|boolean|csv|character varying";
        inputs.forEach( (k, v) -> {
                if (validDataTypes.indexOf(v.getDataType()) < 0 ) {
                    ctx.buildConstraintViolationWithTemplate(
                        String.format("Invalid data type for %s. Must be one of %s", k, validDataTypes))
                    .addConstraintViolation();
                }

                if(v.getDataType().equals("boolean") && v.getDefaultValue() != null) {
                    if (!(v.getDefaultValue().equals("F") || v.getDefaultValue().equals("T"))) {
                        ctx.buildConstraintViolationWithTemplate(
                            String.format("Invalid default value for %s. Must be one of T or F", k))
                        .addConstraintViolation();
                        valid = false;
                    }
                }
            }
        );
        return valid;
    }
}