package org.ebs.rest.validation;


import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Documented
@Constraint(validatedBy = DesignDefinitionValidator.class)
@Target( { ElementType.PARAMETER })
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidDesignDefinition {
    String message() default "Invalid Definition File";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
