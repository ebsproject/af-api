package org.ebs.rest;

import java.util.Map;

import org.ebs.services.to.DesignDefinition;
import org.ebs.util.brapi.BrResponse;
import org.springframework.http.ResponseEntity;

public interface DesignDefinitionResource {

    ResponseEntity<BrResponse<Map<String,Integer>>> addDesign(DesignDefinition designDefinition);
}