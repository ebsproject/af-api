package org.ebs.rest;

import java.util.Collections;
import java.util.Map;

import javax.validation.Valid;

import org.ebs.rest.validation.ValidDesignDefinition;
import org.ebs.services.DesignModelService;
import org.ebs.services.to.DesignDefinition;
import org.ebs.util.brapi.BrResponse;
import org.ebs.util.brapi.BrapiResponseBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("rest")
@RequiredArgsConstructor(onConstructor = @__({@Autowired}))
class DesignDefinitionResourceImpl implements DesignDefinitionResource {

    private final DesignModelService designModelService;

    @Override
    @PostMapping("design-definition")
    public ResponseEntity<BrResponse<Map<String,Integer>>> addDesign( @RequestBody
            @Valid @ValidDesignDefinition DesignDefinition designDefinition) {
        
        int id = designModelService.createDesignDefinition(designDefinition);

        return new ResponseEntity<BrResponse<Map<String,Integer>>>(BrapiResponseBuilder
            .forData(Collections.singletonMap("id", id))
            .withStatusSuccess()
            .build()
            , HttpStatus.OK);
    }

}