package org.ebs.graphql;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import com.coxautodev.graphql.tools.GraphQLResolver;

import org.ebs.services.PropertyService;
import org.ebs.services.to.Property;
import org.ebs.services.to.PropertyMeta;
import org.ebs.services.to.PropertyRule;
import org.ebs.services.to.PropertyUi;
import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
class PropertyResolver implements GraphQLResolver<Property> {

    private final PropertyService propertyService;

    public List<Property> getChildren(Property p) {
        return propertyService.findCatalogElements(p);
    }

    public PropertyUi getUi(Property p) {
        int parentId = Optional.ofNullable(p.getParentPropertyId()).orElse(0);
        return propertyService.findPropertyUi(parentId, p.getId()).orElse(null);
    }

    public Set<PropertyRule> getRules(Property p) {
        int parentId = Optional.ofNullable(p.getParentPropertyId()).orElse(0);
        return propertyService.findPropertyRules(parentId, p.getId());
    }

    public Set<PropertyMeta> getMeta(Property p) {
        return propertyService.findPropertyMeta(p.getId());
    }
}