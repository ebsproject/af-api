package org.ebs.graphql;

import javax.validation.Valid;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;

import org.ebs.graphql.validation.ValidRequestInput;
import org.ebs.services.RequestService;
import org.ebs.services.to.Request;
import org.ebs.services.to.RequestFileDesignInput;
import org.ebs.services.to.RequestInput;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import lombok.RequiredArgsConstructor;

@Component
@Validated
@RequiredArgsConstructor
class MutationResolver implements GraphQLMutationResolver {

    private final RequestService requestService;

    public Request createRequest(@Valid
    @ValidRequestInput
    RequestInput requestInput) {
        return requestService.createRequest(requestInput);
    }

    public Request createFileDesignRequest(@Valid
    RequestFileDesignInput requestInput) {
        return requestService.createFileDesignRequest(requestInput);
    }

}