package org.ebs.graphql;

import static java.util.stream.Collectors.toList;

import java.util.List;
import com.coxautodev.graphql.tools.GraphQLResolver;

import org.ebs.services.to.Request;
import org.ebs.services.to.RequestParameter;
import org.springframework.stereotype.Component;

@Component
public class RequestResolver implements GraphQLResolver<Request>{

    public List<RequestParameter> getParameters(final Request r) {
        return r.getParameters().entrySet().stream()
            .map(e -> new RequestParameter(e.getKey(),e.getValue().toString()))
            .collect(toList());
    }
}