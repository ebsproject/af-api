package org.ebs.graphql.validation;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.ebs.model.CatalogProperty;
import org.ebs.services.PropertyService;
import org.ebs.services.to.Property;
import org.ebs.services.to.Request;
import org.ebs.services.to.RequestInput;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class RequestInputValidator implements ConstraintValidator<ValidRequestInput, RequestInput> {

    private final PropertyService propertyService;

    @Override
    public boolean isValid(RequestInput requestInput, ConstraintValidatorContext ctx) {

        if (requestInput.getType().equals(Request.TRIAL_DESIGN)) {
            Optional<Property> method = checkValidDesignMethod(requestInput, ctx);
            return method.isPresent()
                    ? checkRequestParameters(requestInput, method.get(), ctx)
                            && checkNRepRequired(requestInput, method.get(), ctx)
                    : false;
        }
        ctx.buildConstraintViolationWithTemplate("Type of request not valid")
                .addConstraintViolation();

        return false;
    }

    /**
     * Checks a request for design has a valid method: alpha lattice, rcbd, etc
     * 
     * @param requestInput
     * @param ctx
     * @return the property representing the Design Method to use for the given
     *         request
     */
    private Optional<Property> checkValidDesignMethod(RequestInput requestInput,
            ConstraintValidatorContext ctx) {
        Optional<Property> method = propertyService.findPropertyByCode(requestInput.getMethod(),
                CatalogProperty.CATALOG.DESIGN_MODEL.id());
        if (!method.isPresent()) {
            ctx.buildConstraintViolationWithTemplate(
                    "Design Method does not exists: " + requestInput.getMethod())
                    .addConstraintViolation();
        }

        return method;
    }

    /**
     * Validates a request contains at least all the required input parameters
     * defined in the selected design model
     * 
     * @param requestInput
     * @param method
     * @param ctx
     */
    private boolean checkRequestParameters(RequestInput requestInput, Property method,
            ConstraintValidatorContext ctx) {
        boolean valid = true;
        Set<Property> requiredParameters = propertyService.findCatalogElements(method)
                // gets required input properties for the request
                .stream().filter(p -> p.getType().equals("input") && p.isRequired())
                // required input properties to be ignored
                .filter(p -> !p.getCode().equalsIgnoreCase("entryList"))
                .collect(Collectors.toSet());

        Map<String, String> parameterMap = new HashMap<>();
        requestInput.getParameters().forEach(p -> parameterMap.put(p.getCode(), p.getVal()));

        for (Property required : requiredParameters) {
            if (!parameterMap.keySet().contains(required.getCode())) {
                ctx.buildConstraintViolationWithTemplate(
                        String.format("Required parameter missing: %s (%s)", required.getCode(),
                                required.getName()))
                        .addConstraintViolation();
                valid = false;
            } else {
                String paramVal = parameterMap.get(required.getCode());
                try {
                    switch (required.getDataType()) {
                    case "integer":
                        Integer.parseInt(paramVal);
                        break;
                    case "boolean":
                        if (!paramVal.equalsIgnoreCase("true")
                                && !paramVal.equalsIgnoreCase("false"))
                            throw new Exception();
                    }
                } catch (Exception e) {
                    ctx.buildConstraintViolationWithTemplate(
                            String.format("Parameter %s has invalid %s value: %s",
                                    required.getCode(), required.getDataType(), paramVal))
                            .addConstraintViolation();
                    valid = false;
                }
            }
        }
        return valid;
    }

    private boolean checkNRepRequired(RequestInput requestInput, Property method,
            ConstraintValidatorContext ctx) {
        if (needsNRepInEntries(method.getCode()) && requestInput.getEntryList().getNrep() == null) {
            ctx.buildConstraintViolationWithTemplate("Entry list needs nRep")
                    .addConstraintViolation();
            return false;
        }
        return true;
    }

    public static final boolean needsNRepInEntries(String method) {
        return method.equals(CatalogProperty.randPREPirri)
                || method.equals(CatalogProperty.randAugUnrep);
    }

}