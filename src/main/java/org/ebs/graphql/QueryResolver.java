package org.ebs.graphql;

import java.util.List;

import javax.validation.constraints.Min;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;

import org.ebs.model.CatalogProperty;
import org.ebs.services.PropertyService;
import org.ebs.services.RequestService;
import org.ebs.services.to.Property;
import org.ebs.services.to.Request;
import org.ebs.util.FilterInput;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import lombok.RequiredArgsConstructor;

@Component
@Validated
@RequiredArgsConstructor
class QueryResolver implements GraphQLQueryResolver {

    private final PropertyService propertyService;
    private final RequestService requestService;

    public Property findCatalog(CatalogProperty.CATALOG catalog, List<FilterInput> filters) {
        for (FilterInput f : filters) {
            f.setCol("configProperty." + f.getCol());
        }
        return propertyService.findCatalog(catalog.id(), filters).orElse(null);
    }

    public Property findProperty(@Min(0)
    int propertyId, @Min(0)
    int parentPropertyId) {
        return propertyService.findProperty(propertyId, parentPropertyId).orElse(null);
    }

    public Request findRequest(String uuid) {
        return requestService.findByUuid(uuid);
    }

}