package org.ebs.services.converter;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.ebs.model.AfProperty;
import org.ebs.model.AfRequest;
import org.ebs.model.AfRequestParameter;
import org.ebs.model.repos.AfPropertyRepository;
import org.ebs.services.to.ParameterInput;
import org.ebs.services.to.RequestInput;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class RequestInputToAfRequestConverterTest {

    @Mock
    private AfPropertyRepository propRepoMock;
    @Mock
    private AfProperty propMock;

    private RequestInputToAfRequestConverter subject;

    @BeforeEach
    public void init() {
        subject = new RequestInputToAfRequestConverter(propRepoMock);
    }

    @Test
    public void whenObjectNotNull_ThenCanConvert(){
        Mockito.when(propRepoMock.findByIdAndDeletedIsFalse(anyInt()))
            .thenReturn(Optional.of(propMock));
        Mockito.when(propRepoMock.findByCodeAndDeletedIsFalse(anyString()))
            .thenReturn(Optional.of(propMock));

        RequestInput object = Stub.ofRequestInput();
        AfRequest result = subject.convert(object);

        assertThat(result).extracting("category", "type", "method",
                "design", "requestorId", "institute", "crop", "program")
            .containsExactly("a cat", "a type", propMock,
                "my design", "5", "my inst", "crop a", "a prog");
        assertThat(result.getParameters()).isNotNull()
            .size().isEqualTo(6); // 2 params + 1 extId + 3 occIds
    }

    @Test
    public void givenSingleValueParam_whenMakeParamValues_thenReturnOneParam() {
        Mockito.when(propRepoMock.findByCodeAndDeletedIsFalse(anyString()))
            .thenReturn(Optional.of(propMock));

        ParameterInput object = Stub.ofParamInput("1", "a val");
        List<AfRequestParameter> result = subject.makeParamValues(object);
        
        assertThat(result).size().isEqualTo(1);
        assertThat(result.get(0))
            .extracting("property", "value")
            .containsExactly(propMock, "a val");
    }

    @Test
    public void givenMultiValueParam_whenMakeParamValues_thenReturnMultipleParams() {
        Mockito.when(propRepoMock.findByCodeAndDeletedIsFalse(anyString()))
            .thenReturn(Optional.of(propMock));

        ParameterInput object = Stub.ofParamInput("1", "1", "2", "3");
        List<AfRequestParameter> result = subject.makeParamValues(object);
        
        assertThat(result).size().isEqualTo(3);
        assertThat(result).extracting("value")
            .containsExactly("1", "2", "3");
        assertThat(result).extracting("property")
            .contains(propMock, propMock, propMock);
    }

    @Test
    public void givenPropertyNotExist_whenMakeParamValues_thenThrowException() {
        Mockito.when(propRepoMock.findByCodeAndDeletedIsFalse(anyString()))
            .thenReturn(Optional.empty());
        
        ParameterInput object = Stub.ofParamInput("bad_prop", "a val");
        
        assertThatExceptionOfType(RuntimeException.class)
            .isThrownBy(() -> subject.makeParamValues(object))
            .withMessage("invalid property: bad_prop");    
    }

    @Test
    public void givenNotNullRequest_whenAddExperimentParameters_thenAExperimentIdsAsParameters(){
        Mockito.when(propRepoMock.findByIdAndDeletedIsFalse(anyInt()))
            .thenReturn(Optional.of(propMock));

        RequestInput ri  = Stub.ofRequestInput();
        AfRequest r = new AfRequest();
        r.setParameters(new ArrayList<>());

        subject.addExperimentParameters(ri, r);

        assertThat(r.getParameters())
            .size().isEqualTo(4);
        assertThat(r.getParameters()).extracting("property")
            .containsOnly(propMock);
        assertThat(r.getParameters()).extracting("value")
            .containsExactlyInAnyOrder("11", "1", "2", "3");
    }

}