package org.ebs.services.converter;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import org.ebs.model.AfPropertyConfig;
import org.ebs.model.AfPropertyUi;
import org.ebs.services.to.DesignDefinition.BOOL;
import org.ebs.services.to.DesignDefinitionParameter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class DesignDefinitionParameterToAfPropertyConfigConverterTest {

    @Mock
    private DesignDefinitionParameterToAfPropertyUiConverter uiConverterMock;
    @Mock
    private AfPropertyUi propertyUiMock;
    private DesignDefinitionParameterToAfPropertyConfigConverter subject;

    @BeforeEach
    public void init() {
        subject = new DesignDefinitionParameterToAfPropertyConfigConverter(uiConverterMock);
    }

    @Test
    public void whenObjectNotNull_thenCanConvert(){
        when(uiConverterMock.convert(any()))
            .thenReturn(propertyUiMock);

        DesignDefinitionParameter input = new DesignDefinitionParameter();
        input.setIsLayoutVariable(BOOL.T);
        input.setOrderNumber(2);
        input.setRequired(BOOL.T);
        
        AfPropertyConfig result = subject.convert(input);
        
        assertThat(result)
            .extracting("layoutVariable", "orderNumber", "required","ui")
            .containsExactly(true,2,true, propertyUiMock);
    }
}