package org.ebs.services.converter;

import static org.assertj.core.api.Assertions.assertThat;

import org.ebs.model.AfProperty;
import org.ebs.services.to.DesignDefinitionParameter;
import org.junit.jupiter.api.Test;

public class DesignDefinitionParameterToAfPropertyConverterTest {

    private final DesignDefinitionParameterToAfPropertyConverter subject = new DesignDefinitionParameterToAfPropertyConverter();

    @Test
    public void whenObjectNotNull_thenCanConvert(){
        DesignDefinitionParameter input = new DesignDefinitionParameter();
        input.setDataType("a dataType");
        input.setDescription("a desc");
        input.setVariableAbbrev("a variableAbbrev");
        input.setVariableLabel("a label");

        AfProperty result = subject.convert(input);

        assertThat(result).extracting("dataType", "description", "name", "variableLabel", "type")
            .containsExactly("a dataType","a desc","a variableAbbrev","a label","input");
    }
}