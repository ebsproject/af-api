package org.ebs.services.converter;

import static org.assertj.core.api.Assertions.assertThat;

import org.ebs.model.AfPropertyUi;
import org.ebs.services.to.PropertyUi;
import org.junit.jupiter.api.Test;

public class AfPropertyUiToPropertyUiConverterTest {

    private final AfPropertyUiToPropertyUiConverter subject = new AfPropertyUiToPropertyUiConverter();

    @Test
    public void whenObjectNotNull_thenCanConvert(){

        AfPropertyUi object = Stub.ofAfPropertyUi();
        PropertyUi result = subject.convert(object);

        assertThat(result).extracting("id", "visible", "minimum", "maximum", "unit", "defaultValue",
                "disabled", "multiple", "catalogue")
            .containsExactly(2,true,1,99999,"cm","def val",false,true,true);
    }

}