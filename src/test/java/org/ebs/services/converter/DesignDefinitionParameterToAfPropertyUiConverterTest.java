package org.ebs.services.converter;

import static org.assertj.core.api.Assertions.assertThat;

import org.ebs.model.AfPropertyUi;
import org.ebs.services.to.DesignDefinitionParameter;
import org.ebs.services.to.DesignDefinition.BOOL;
import org.junit.jupiter.api.Test;

public class DesignDefinitionParameterToAfPropertyUiConverterTest {

    private final DesignDefinitionParameterToAfPropertyUiConverter subject = new DesignDefinitionParameterToAfPropertyUiConverter();

    @Test
    public void whenObjectNotNull_thenCanConvert(){
        DesignDefinitionParameter input = new DesignDefinitionParameter();
        input.setDefaultValue("a defaultValue");
        input.setDisabled(BOOL.T);
        input.setMaximum(15);
        input.setMinimum(2);
        input.setUnit("a unit");
        input.setVisible(BOOL.T);

        AfPropertyUi result = subject.convert(input);

        assertThat(result).extracting("catalogue", "defaultValue", "disabled", "maximum",
                "minimum", "multiple", "unit", "visible")
            .containsExactly(false, "a defaultValue", true, 15, 2, false, "a unit", true);
    }
}