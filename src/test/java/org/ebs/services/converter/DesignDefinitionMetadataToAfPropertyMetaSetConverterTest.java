package org.ebs.services.converter;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.groups.Tuple.tuple;

import java.sql.Date;
import java.util.Set;

import org.ebs.model.AfPropertyMeta;
import org.ebs.services.to.DesignDefinitionMetadata;
import org.junit.jupiter.api.Test;

public class DesignDefinitionMetadataToAfPropertyMetaSetConverterTest {

    private final DesignDefinitionMetadataToAfPropertyMetaSetConverter subject = new DesignDefinitionMetadataToAfPropertyMetaSetConverter();

    @Test
    public void whenObjectNotNull_thenCanConvert(){
        DesignDefinitionMetadata input = stubDesignDefinitionMetadata();
        Set<AfPropertyMeta> result = subject.convert(input);

        assertThat(result.size()).isEqualTo(9);

        assertThat(result).extracting("code", "value").containsExactlyInAnyOrder(
            tuple("author","my author"),
            tuple("date","2020-07-24"),
            tuple("email","some@email"),
            tuple("engine","engine 1.2"),
            tuple("modelVersion","123"),
            tuple("note","a note"),
            tuple("organizationCode","org_code"),
            tuple("rversion","1.2.3"),
            tuple("syntax","my syntax"));
    }

    @Test
    public void givenLongSyntax_whenConvert_thenReturnSplittedSyntax(){
        DesignDefinitionMetadata input = new DesignDefinitionMetadata();
        input.setSyntax("my syntax A |  my syntax B -b   |   my syntax 'C'");
        Set<AfPropertyMeta> result = subject.convert(input);

        //model version is int, so it's never null
        assertThat(result.size()).isEqualTo(4);

        assertThat(result).extracting("code", "value").contains(
            tuple("syntax1","my syntax A"),
            tuple("syntax2","my syntax B -b"),
            tuple("syntax3","my syntax 'C'"));
    }

    private DesignDefinitionMetadata stubDesignDefinitionMetadata() {
        DesignDefinitionMetadata stub = new DesignDefinitionMetadata();
        stub.setAuthor("my author");
        stub.setDate(Date.valueOf("2020-07-24"));
        stub.setDesign("a design");
        stub.setEmail("some@email");
        stub.setEngine("engine 1.2");
        stub.setMethod("a method");
        stub.setModelVersion(123);
        stub.setNote("a note");
        stub.setOrganizationCode("org_code");
        stub.setRversion("1.2.3");
        stub.setSyntax("my syntax");
        return stub;
    }




}