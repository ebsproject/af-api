package org.ebs.services.converter;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.UUID;

import org.ebs.model.AfProperty;
import org.ebs.model.AfPropertyConfig;
import org.ebs.model.AfPropertyUi;
import org.ebs.model.AfRequest;
import org.ebs.model.AfRequestEntry;
import org.ebs.model.AfRequestParameter;
import org.ebs.services.to.EntryListInput;
import org.ebs.services.to.ParameterInput;
import org.ebs.services.to.Request;
import org.ebs.services.to.RequestInput;
import org.ebs.services.to.RequestStatus;

/**
 * Stub objects for Converter's tests
 */
class Stub {
    public static final Instant FIXED_INSTANT = Instant.ofEpochSecond(9000000L);
    public static final String FIXED_UUID = UUID.fromString("684210fb-fc85-463b-a378-4e8ff2b42789").toString();

    public static final RequestInput ofRequestInput() {
        RequestInput r = new RequestInput();

        r.setCategory("a cat");
        r.setType("a type");
        r.setMethod("123");
        r.setDesign("my design");
        r.setRequestorId("5");
        r.setOrganization_code("my inst");
        r.setCrop("crop a");
        r.setProgram("a prog");
        r.setExperiment_id(11);
        r.setOccurrence_id(Arrays.asList(1L,2L,3L));
        r.setParameters(new ArrayList<>());
        r.getParameters().add(ofParamInput("1", "a val"));
        r.getParameters().add(ofParamInput("2", "other val"));
        r.setEntryList(new EntryListInput());
        r.setEntryList(ofEntryListInput());

        return r;
    }

    public static final EntryListInput ofEntryListInput() {
        EntryListInput list = new EntryListInput();
        list.setEntry_class(new String[]{"",""});
        list.setEntry_id(new long[]{10,11});
        list.setEntry_name(new String[]{"n1","n2"});
        list.setEntry_number(new int[]{1,2});
        list.setEntry_role(new String[]{"check", "entry"});
        list.setEntry_status(new String[]{"active", "active"});
        list.setEntry_type(new String[]{"check", "entry"});
        list.setGermplasm_id(new long[]{111,112});

        return list;
    }

    public static final ParameterInput ofParamInput(String code, String val) {
        ParameterInput p = new ParameterInput();
        p.setCode(code);
        p.setVal(val);
        return p;
    }
    public static final ParameterInput ofParamInput(String code, String... vals) {
        ParameterInput p = new ParameterInput();
        p.setCode(code);
        p.setVals(vals);
        return p;
    }

    public static final AfPropertyUi ofAfPropertyUi(){
        AfPropertyUi p = new AfPropertyUi();
        p.setCatalogue(true);
        p.setDefaultValue("def val");
        p.setDeleted(false);
        p.setDisabled(false);
        p.setId(2);
        p.setMaximum(99999);
        p.setMinimum(1);
        p.setMultiple(true);
        p.setUnit("cm");
        p.setVisible(true);
        return p;
    }

    public static final AfRequest ofAfRequest(){
        AfRequest r = new AfRequest();
        
        
        r.setCategory("a cat");
        r.setCrop("a crop");
        r.setDeleted(false);
        r.setDesign("a design");
        r.setId(1122);
        r.setInstitute("some inst");
        r.setMethod(ofAfProperty(6,"methodProp","string","a design"));
        r.setProgram("my program");
        r.setRequestorId("7");
        r.setStatus(RequestStatus.created);
        r.setType(Request.TRIAL_DESIGN);
        r.setUuid(Stub.FIXED_UUID);
        r.setCreatedOn(Date.from(FIXED_INSTANT));

        AfProperty exptIdProp = ofAfProperty(1,"experimentId","integer","name 2")
            , occIdProp = ofAfProperty(2,"occurrenceId","integer","name 2")
            , intProp = ofAfProperty(3,"intProp","integer","name 3")
            , stringProp = ofAfProperty(4,"stringProp","string","name 4")
            , booleanProp = ofAfProperty(5,"booleanProp","boolean","name 5");

        r.setParameters(new ArrayList<>());
        r.getParameters().add(ofAfRequestParameter(1,exptIdProp,r,"11220"));
        r.getParameters().add(ofAfRequestParameter(2,occIdProp,r,"100"));
        r.getParameters().add(ofAfRequestParameter(3,occIdProp,r,"101"));
        r.getParameters().add(ofAfRequestParameter(4,intProp,r,"5"));
        r.getParameters().add(ofAfRequestParameter(5,booleanProp,r,"true"));
        r.getParameters().add(ofAfRequestParameter(6,stringProp,r,"some value"));

        r.setEntries(Arrays.asList(new AfRequestEntry(101, 1111, 123, r, 1)
            , new AfRequestEntry(102, 1111, 456, r, 2)));

        return r;
    }

    public static final AfRequest ofAfRequestForDesignFile() {
        AfRequest r = new AfRequest();
        r.setId(123);
        r.setStatus(RequestStatus.created);
        r.setUuid(Stub.FIXED_UUID);
        r.setCreatedOn(Date.from(FIXED_INSTANT));

        AfProperty exptIdProp = ofAfProperty(1,"experimentId","integer","name 2")
            , occIdProp = ofAfProperty(2,"occurrenceId","integer","name 2");

        r.setParameters(new ArrayList<>());
        r.getParameters().add(ofAfRequestParameter(1,exptIdProp,r,"123"));
        r.getParameters().add(ofAfRequestParameter(2,occIdProp,r,"111"));
        r.getParameters().add(ofAfRequestParameter(3,occIdProp,r,"222"));

        r.setEntries(Arrays.asList(new AfRequestEntry(101, 1111, 123, r, 1)
            , new AfRequestEntry(102, 1111, 456, r, 2)));

        return r;
    }

    public static final AfProperty ofAfProperty(int id, String code, String dataType, String name) {
        AfProperty p = new AfProperty(id);
        p.setCode(code);
        p.setDataType(dataType);
        p.setName(name);
        return p;
    }

    public static final AfRequestParameter ofAfRequestParameter(int id, AfProperty prop, AfRequest req,
            String val) {
        AfRequestParameter  p = new AfRequestParameter ();
        p.setDeleted(false);
        p.setId(id);
        p.setProperty(prop);
        p.setRequest(req);
        p.setValue(val);
        return p;
    }

    public static final AfPropertyConfig ofAfPropertyConfigObject(){
        AfProperty p = ofAfProperty(456, "a code", "integer", "a name");
        p.setDeleted(false);
        p.setDescription("a desc");
        p.setType("input");
        p.setVariableLabel("my label");

        AfPropertyConfig c = new AfPropertyConfig();
        c.setDeleted(false);
        c.setId(999);
        c.setOrderNumber(3);
        c.setRequired(true);
        c.setProperty(new AfProperty(11));
        c.setConfigProperty(p);
        c.setLayoutVariable(true);

        return c;
    }
}