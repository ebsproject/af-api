package org.ebs.services.converter;

import static org.assertj.core.api.Assertions.assertThat;
import static org.ebs.services.converter.Stub.FIXED_INSTANT;
import static org.ebs.util.DateTimeCoercing.DATETIME_FORMAT;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.ebs.model.AfRequest;
import org.ebs.model.AfRequestParameter;
import org.ebs.services.to.Request;
import org.ebs.services.to.RequestEntry;
import org.ebs.services.to.RequestMetadata;
import org.ebs.util.DateTimeCoercing;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class AfRequestToRequestConverterTest {


    @Mock AfRequestEntryToRequestEntryConverter requestEntryConverterMock;

    private AfRequestToRequestConverter subject;

    @BeforeEach
    public void init() {
        subject = new AfRequestToRequestConverter(requestEntryConverterMock);
    }

    @Test
    public void whenObjectNotNull_thenCanConvert(){

        AfRequest object = Stub.ofAfRequest();
        
        Request result = subject.convert(object);

        assertThat(result.getMetadata()).as("metadata assertion")
            .extracting("category", "type", "method",
                "design", "timestamp", "id",
                "requestorId", "organization_code", "crop",
                "program", "experiment_id", "occurrence_id")
            .containsExactly("a cat", "Trial Design", "methodProp",
                "a design", DateTimeCoercing.DATETIME_FORMAT.format(Stub.FIXED_INSTANT), "684210fb-fc85-463b-a378-4e8ff2b42789_SD_0000",
                 "7","some inst", "a crop",
                 "my program", 11220L, Arrays.asList(100L,101L));
    }
    
    @Test
    public void whenObjectNotNull_thenSetParameterValues() {
        Request object = new Request();
        subject.setParameterValues(Stub.ofAfRequest(), object);
        
        assertThat(object.getParameters())
        .size().isEqualTo(3);
        assertThat(object.getParameters())
        .containsValues(5, "some value", "T");
        assertThat(object.getParameters())
        .containsKeys("intProp", "stringProp", "booleanProp");
    }
    
    @Test
    public void givenIntegerTypeParameter_whenValueForParameter_thenReturnIntegerValue() {
        AfRequestParameter object = Stub.ofAfRequestParameter(4,Stub.ofAfProperty(1, "a code", "integer", "a name"),null,"5");
        
        Object result = subject.valueForParameter(object);
        
        assertThat(result).isEqualTo(5);
    }
    
    @Test
    public void givenBooleanTypeParameter_whenValueForParameter_thenReturnBooleanValue() {
        AfRequestParameter object = Stub.ofAfRequestParameter(4,Stub.ofAfProperty(1, "a code", "boolean", "a name"),null,"true");
        
        Object result = subject.valueForParameter(object);
        
        assertThat(result).isEqualTo("T");
    }
    
    @Test
    public void givenStringTypeParameter_whenValueForParameter_thenReturnStringValue() {
        AfRequestParameter object = Stub.ofAfRequestParameter(4,Stub.ofAfProperty(1, "a code", "string", "a name"),null,"my value");
        
        Object result = subject.valueForParameter(object);
        
        assertThat(result).isEqualTo("my value");
    }
    
    @Test
    public void whenObjectNotNull_thenSetExptAndOccMetadata() {
        RequestMetadata target = new RequestMetadata();
        Map<String,List<Object>> object = new HashMap<>();
        object.put("experimentId", Arrays.asList(123));
        object.put("occurrenceId", Arrays.asList(11,22,33,44));
        
        subject.setExptAndOccMetadata(target, object);
        
        assertThat(target.getExperiment_id())
        .isEqualTo(123);
        assertThat(target.getOccurrence_id())
        .containsExactly(11L, 22L, 33L, 44L);
        assertThat(object).isEmpty();
        
    }
    
    @Test
    public void givenRequestForDesignFile_whenConvert_thenCanConvert(){

        AfRequest object = Stub.ofAfRequestForDesignFile();
        
        Request result = subject.convert(object);

        assertThat(result.getMetadata()).as("metadata assertion")
            .extracting("category", "type", "method",
                "design", "timestamp", "id",
                "requestorId", "organization_code", "crop",
                "program", "experiment_id", "occurrence_id")
            .containsExactly(null, null, null,
                null, DATETIME_FORMAT.format(FIXED_INSTANT), "684210fb-fc85-463b-a378-4e8ff2b42789_SD_0000",
                null,null, null,
                null, 123L, Arrays.asList(111L,222L));
    }

    @Test
    public void givenNotEmptyEntryList_whenSetEntryList_thenConvertEntries() {
        when(requestEntryConverterMock.convert(any())).thenReturn(new RequestEntry());

        Request request = new Request();
        subject.setEntryList(Stub.ofAfRequest(), request);

        assertThat(request.getEntryList()).hasSize(2);
        verify(requestEntryConverterMock,times(2)).convert(any());
    }
}