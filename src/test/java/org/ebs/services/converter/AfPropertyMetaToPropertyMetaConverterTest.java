package org.ebs.services.converter;

import static org.assertj.core.api.Assertions.assertThat;

import org.ebs.model.AfPropertyMeta;
import org.ebs.services.to.PropertyMeta;
import org.junit.jupiter.api.Test;

public class AfPropertyMetaToPropertyMetaConverterTest {

    private final AfPropertyMetaToPropertyMetaConverter subject = new AfPropertyMetaToPropertyMetaConverter();

    @Test
    public void whenObjectNotNull_thenCanConvert(){
        
        AfPropertyMeta object = initAfPropertyMeta();
        PropertyMeta result = subject.convert(object);

        assertThat(result).extracting("code","value")
            .containsExactly("a code","my value");
    }

    private AfPropertyMeta initAfPropertyMeta() {
        AfPropertyMeta m = new AfPropertyMeta();
        m.setCode("a code");
        m.setValue("my value");
        return m;
    }
}