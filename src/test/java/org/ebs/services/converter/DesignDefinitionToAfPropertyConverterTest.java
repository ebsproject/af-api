package org.ebs.services.converter;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.util.Set;

import org.ebs.model.AfProperty;
import org.ebs.model.AfPropertyMeta;
import org.ebs.services.to.DesignDefinition;
import org.ebs.services.to.DesignDefinitionMetadata;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class DesignDefinitionToAfPropertyConverterTest {

    @Mock
    private DesignDefinitionMetadataToAfPropertyMetaSetConverter metaConverterMock;
    @Mock
    private Set<AfPropertyMeta> metaListMock;

    private DesignDefinitionToAfPropertyConverter subject;

    @BeforeEach
    public void init() {
        subject = new DesignDefinitionToAfPropertyConverter(metaConverterMock);
    }

    @Test
    public void whenObjectNotNull_ThenCanConvert() {
        when(metaConverterMock.convert(any())).thenReturn(metaListMock);

        DesignDefinition input = new DesignDefinition();
        input.setMetadata(new DesignDefinitionMetadata());
        input.getMetadata().setMethod("a method");
        input.getMetadata().setDesign("a design");

        AfProperty result = subject.convert(input);

        assertThat(result).extracting("type", "dataType", "code", "name", "meta")
                .containsExactly("catalog_item", "integer", "a method", "a design", metaListMock);

    }

}