package org.ebs.services.converter;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.UUID;

import org.ebs.model.AfRequest;
import org.ebs.services.to.BrRequest;
import org.ebs.services.to.RequestStatus;
import org.junit.jupiter.api.Test;

public class AfToBrRequestConverterTest {

    private final AfToBrRequestConverter subject = new AfToBrRequestConverter();
    private static final String fixedUuid = UUID.randomUUID().toString();

    @Test
    public void whenObjectNotNull_thenCanConvert(){
        
        AfRequest object = initAfRequest();
        BrRequest result = subject.convert(object);

        assertThat(result).extracting("id", "uuid", "category", "status")
            .containsExactly(123,fixedUuid,"a category", RequestStatus.created);
    }

    private AfRequest initAfRequest() {
        AfRequest r = new AfRequest();
        r.setId(123);
        r.setUuid(fixedUuid);
        r.setCategory("a category");
        r.setStatus(RequestStatus.created);

        return r;
    }
}