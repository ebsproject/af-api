package org.ebs.services.converter;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Optional;

import org.ebs.model.AfProperty;
import org.ebs.model.AfRequest;
import org.ebs.model.repos.AfPropertyRepository;
import org.ebs.services.to.RequestFileDesignInput;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class RequestFileDesignInputToAfRequestConverterTest {

    @Mock
    private AfPropertyRepository propertyRepoMock;

    private RequestFileDesignInputToAfRequestConverter subject;

    @BeforeEach
    public void init() {
        subject = new RequestFileDesignInputToAfRequestConverter(propertyRepoMock);
    }

    @Test
    public void givenSourceIsNotNull_whenConvert_thenSuccess() {
        when(propertyRepoMock.findByIdAndDeletedIsFalse(anyInt())).thenReturn(Optional.of(new AfProperty(1)));

        RequestFileDesignInput source = new RequestFileDesignInput();
        source.setOccurrenceIds(Arrays.asList(111,222));
        source.setEntryIds(Arrays.asList(111,222,333,444,555));
        source.setDesignFileName("a designFileName");
        
        AfRequest result = subject.convert(source);

        assertThat(result.getParameters()).size().isEqualTo(3); //2 occ ids + 1 expt id
        assertThat(result.getEntries()).size().isEqualTo(5);
        assertEquals("a designFileName", result.getDesign());

        assertThat(result.getEntries()).extracting("entryId", "entryNumber").containsExactly(
            tuple(111L,1),
            tuple(222L,2),
            tuple(333L,3),
            tuple(444L,4),
            tuple(555L,5));
    }

}
