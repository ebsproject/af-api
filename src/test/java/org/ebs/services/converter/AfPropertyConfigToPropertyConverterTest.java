package org.ebs.services.converter;

import static org.assertj.core.api.Assertions.assertThat;

import org.ebs.model.AfPropertyConfig;
import org.ebs.services.to.Property;
import org.junit.jupiter.api.Test;

public class AfPropertyConfigToPropertyConverterTest {

    private final AfPropertyConfigToPropertyConverter subject = new AfPropertyConfigToPropertyConverter();

    @Test
    public void whenObjectNotNull_thenCanConvert(){
        
        AfPropertyConfig object = Stub.ofAfPropertyConfigObject();
        Property result = subject.convert(object);

        assertThat(result).extracting("id", "code", "name", "variableLabel", "description", "type", "dataType"
            , "parentPropertyId", "orderNumber", "required","layoutVariable")
            .containsExactly(456,"a code","a name", "my label", "a desc", "input", "integer", 11, 3, true, true);
    }
}