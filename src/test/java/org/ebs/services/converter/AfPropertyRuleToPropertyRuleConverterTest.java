package org.ebs.services.converter;

import static org.assertj.core.api.Assertions.assertThat;

import org.ebs.model.AfPropertyConfig;
import org.ebs.model.AfPropertyRule;
import org.ebs.services.to.PropertyRule;
import org.junit.jupiter.api.Test;

public class AfPropertyRuleToPropertyRuleConverterTest {

    private final AfPropertyRuleToPropertyRuleConverter subject = new AfPropertyRuleToPropertyRuleConverter();

    @Test
    public void whenObjectNotNull_thenCanConvert(){
        
        AfPropertyRule object = initAfPropertyRule();
        PropertyRule result = subject.convert(object);

        assertThat(result).extracting("id", "propertyId", "type", "expression", "group")
            .containsExactly(0,123,"a type","some expr",2);
    }

    private AfPropertyRule initAfPropertyRule() {
        AfPropertyRule r = new AfPropertyRule();
        r.setDeleted(false);
        r.setExpression("some expr");
        r.setGroup(2);
        r.setPropertyConfig(new AfPropertyConfig());
        r.setPropertyId(123);
        r.setType("a type");
        return r;
    }
}