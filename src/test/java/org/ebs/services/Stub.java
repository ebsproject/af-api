package org.ebs.services;

import static java.util.Collections.singletonList;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.ebs.services.to.BrPlot;
import org.ebs.services.to.DesignArray;
import org.ebs.services.to.ParameterInput;
import org.ebs.services.to.RequestInput;

/**
 * Stub objects for Services' tests
 */
class Stub {
    private static DesignArray rcbdDesign = null;

    public static final RequestInput ofRequestInput() {
        RequestInput r = new RequestInput();

        r.setCategory("a cat");
        r.setType("a type");
        r.setMethod("123");
        r.setDesign("my design");
        r.setRequestorId("5");
        r.setOrganization_code("my inst");
        r.setCrop("crop a");
        r.setProgram("a prog");
        r.setExperiment_id(11);
        r.setOccurrence_id(Arrays.asList(1L,2L,3L));
        r.setParameters(new ArrayList<>());
        r.getParameters().add(ofParamInput("1", "a val"));
        r.getParameters().add(ofParamInput("2", "other val"));

        return r;
    }

    public static final ParameterInput ofParamInput(String code, String val) {
        ParameterInput p = new ParameterInput();
        p.setCode(code);
        p.setVal(val);
        return p;
    }

    public static final DesignArray rcbdDesign() {
        if (rcbdDesign == null) {

            Map<String, List<BrPlot>> plots = new HashMap<>();
            BrPlot plot1 = new BrPlot()
                ,plot2 = new BrPlot();

            plot1.setDesignX("1");
            plot1.setDesignY("1");
            plot1.setEntryDbId("13");
            plot1.setLocationDbId(0);
            plot1.setOccurrenceDbId("654565");
            plot1.setPlotCode("1_101");
            plot1.setPlotNumber(101);
            plot1.setPlotOrderNumber(101);
            plot1.setPlotStatus("active");
            plot1.setPlotType("plot");
            plot1.setRep("1");

            plot2.setDesignX("6");
            plot2.setDesignY("7");
            plot2.setEntryDbId("8");
            plot2.setLocationDbId(0);
            plot2.setOccurrenceDbId("654566");
            plot2.setPlotCode("2_314");
            plot2.setPlotNumber(314);
            plot2.setPlotOrderNumber(314);
            plot2.setPlotStatus("active");
            plot2.setPlotType("plot");
            plot2.setRep("3");

            plots.put("654565",singletonList(plot1));
            plots.put("654566",singletonList(plot2));
            
            Map<String,Integer> rcbdColMap = new HashMap<>();
            rcbdColMap.put("occurrence", 0);
            rcbdColMap.put("plot_number", 1);
            rcbdColMap.put("block", 2);
            rcbdColMap.put("entry_id", 3);
            rcbdColMap.put("field_row", 4);
            rcbdColMap.put("field_col", 5);
    
            rcbdDesign = new DesignArray(plots, rcbdColMap);
        }
        return rcbdDesign;
    }

}