package org.ebs.services;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.groups.Tuple.tuple;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import org.ebs.services.to.BrDesignBlockElement;
import org.ebs.services.to.BrPlot;
import org.ebs.services.to.DesignArray;
import org.ebs.services.to.Request;
import org.ebs.services.to.RequestEntry;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Answers;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class DesignArrayParserTest {

    private DesignArrayParser subject = new DesignArrayParser() ;

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private Request requestMock;

    @Test
    public void givenResultFileExists_whenGetHeaderMap_thenReturnColumnMap() {
        Map<String,Integer> expected = new HashMap<>();
        expected.put("colA", 0);
        expected.put("colB", 1);
        expected.put("colC", 2);
        expected.put("other1", 3);
        expected.put("other2", 4);

        String columns="colA,colB,colC,other1,other2";
        Map<String,Integer> columnMap =subject.getHeaderMap(columns);

        assertThat(columnMap.size()).isEqualTo(5);
        assertThat(columnMap).containsExactlyEntriesOf(expected);
    }

    @Test
    public void givenRowRCBD_whenGeneratePlot_thenReturnRcbdDesign() {
        List<RequestEntry> entryList = Arrays.asList(new RequestEntry(0,250,13), new RequestEntry(0,251,8));

        Scanner scannerStub = new Scanner("occurrence,plot_number,block,entry_id,field_row,field_col\n" +
        "1,101,\"1\",\"250\",1,1\n" +
        "2,314,\"3\",\"251\",7,6");
        when(requestMock.getMetadata().getOccurrence_id().get(anyInt()))
            .thenReturn(654565L,654566L);
        when(requestMock.getEntryList())
            .thenReturn(entryList);
        when(requestMock.getMetadata().getType())
            .thenReturn(Request.TRIAL_DESIGN);

        DesignArray result = subject.generateDesign(scannerStub, requestMock);

        assertThat(result.getPlotList().get("654565"))
        .extracting("designX", "designY", "entryDbId","locationDbId"
            ,"occurrenceDbId", "plotCode", "plotNumber", "plotOrderNumber"
            ,"plotStatus", "plotType", "rep")
        .containsExactly(
            tuple("1","1","250",0L,"654565","101",101,101,"active","plot","1"));

        assertThat(result.getPlotList().get("654566"))
            .extracting("designX", "designY", "entryDbId","locationDbId"
                ,"occurrenceDbId", "plotCode", "plotNumber", "plotOrderNumber"
                ,"plotStatus", "plotType", "rep")
            .containsExactly(
                tuple("6","7","251",0L,"654566","314",314,314,"active","plot","3"));
    }
    
    @Test
    public void givenDesignArray_whenParseDesignTable_thenReturnRCBDTable() {
        List<BrDesignBlockElement> result = subject.parseDesignTable(Stub.rcbdDesign(), getRcbdPlotResult(), requestMock);
        
        assertThat(result).size().isEqualTo(2);

        assertThat(result)
        .extracting("occurrenceDbId", "designDbId", "plotDbId",
            "blockType", "blockValue", "blockLevelNumber")
        .containsExactly(
            tuple("654565","1","1500","replication block","1","1"),
            tuple("654566","3","1501","replication block","3","1"));
    }

    private Map<String,List<BrPlot>> getRcbdPlotResult() {
        Map<String,List<BrPlot>> plots = new HashMap<>();
        plots.put("654565", Collections.singletonList(new BrPlot()));
        plots.put("654566", Collections.singletonList(new BrPlot()));
        plots.get("654565").get(0).setPlotDbId("1500");
        plots.get("654566").get(0).setPlotDbId("1501");
        plots.get("654565").get(0).setRep("1");
        plots.get("654566").get(0).setRep("3");
        return plots;
    }

    @Test
    public void givenRowWithCommas_whenElementsFromRow_thenReturnArray() {
        String[] result = subject.elementsFromRow("header1,header2,header3,header4,header5");
        assertThat(result).containsExactly("header1","header2","header3","header4","header5");
    }

    @Test
    public void givenRowWithTabs_whenElementsFromRow_thenReturnArray() {
        String[] result = subject.elementsFromRow("header1		header2	header3	header4		header5");
        assertThat(result).containsExactly("header1","header2","header3","header4","header5");
    }

    @Test
    public void givenRowWithBlanks_whenElementsFromRow_thenReturnArray() {
        String[] result = subject.elementsFromRow("header1   header2  header3   header4 header5  ");
        assertThat(result).containsExactly("header1","header2","header3","header4","header5");
    }

    @Test
    public void givenTypeTrialDesign_whenExtractEntryId_thenReturnEntryIdFromRow() {
        Map<String,Integer> columnMap = new HashMap<>();
        columnMap.put("entry_id", 1);
        when(requestMock.getMetadata().getType()).thenReturn(Request.TRIAL_DESIGN);
        
        String result = subject.extractEntryId("NA,99,NA".split(","), columnMap, requestMock);

        assertEquals("99", result);
    }

    @Test
    public void givenTypeCustomDesign_whenExtractEntryId_thenReturnEntryIdFromRequest() {
        Map<String,Integer> columnMap = new HashMap<>();
        columnMap.put("entry_id", 1);
        when(requestMock.getMetadata().getType()).thenReturn(Request.CUSTON_DESIGN);
        when(requestMock.getEntryList()).thenReturn(asList(new RequestEntry(1000, 7253, 99)));
        
        String result = subject.extractEntryId("NA,99,NA".split(","), columnMap, requestMock);

        assertEquals("7253", result);
    }

}