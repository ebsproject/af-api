package org.ebs.services;

import static java.time.Instant.now;
import static org.assertj.core.api.Assertions.assertThat;
import static org.ebs.services.to.Request.CUSTON_DESIGN;
import static org.ebs.services.to.Request.TRIAL_DESIGN;
import static org.ebs.services.to.RequestStatus.completed;
import static org.ebs.services.to.RequestStatus.expired;
import static org.ebs.services.to.RequestStatus.failed;
import static org.ebs.services.to.RequestStatus.processing_custom_design;
import static org.ebs.services.to.RequestStatus.submitted;
import static org.ebs.util.DateTimeCoercing.DATETIME_FORMAT;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.nio.file.Path;
import java.util.Arrays;
import java.util.Collections;

import org.ebs.services.to.AEOResponse;
import org.ebs.services.to.AEOStatus;
import org.ebs.services.to.DesignArray;
import org.ebs.services.to.Request;
import org.ebs.services.to.RequestMetadata;
import org.ebs.services.to.RequestStatus;
import org.ebs.util.RestClient;
import org.ebs.util.brapi.BrPagedResponse;
import org.ebs.util.brapi.BrapiClient;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Answers;
import org.mockito.Mock;
import org.mockito.internal.util.reflection.FieldSetter;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class FPOServiceImplTest {

    @Mock
    private RequestService requestService;
    @Mock
    private BrapiClient b4rApiClient;
    @Mock
    private DesignArrayParser parser;
    @Mock
    DesignArray mockDesignArray;
    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    BrPagedResponse<Object> response;
    @Mock
    RestClient mockAeoClient;

    Request aFreshRequest;

    private FPOServiceImpl subject;

    @BeforeEach
    public void init() throws NoSuchFieldException, SecurityException {
        subject = new FPOServiceImpl(requestService, b4rApiClient, parser, mockAeoClient);
        FieldSetter.setField(subject, FPOServiceImpl.class.getDeclaredField("fpoTargetPath"),
                "src/test/resources/example-designs");
        FieldSetter.setField(subject,
                FPOServiceImpl.class.getDeclaredField("expirationTimeMinutes"), 5);

        aFreshRequest = new Request();
        aFreshRequest.setMetadata(new RequestMetadata());
        aFreshRequest.getMetadata().setId("RCBD_cimmyt_SD_0000");
        aFreshRequest.setCreatedBy("some@user.test");
        aFreshRequest.getMetadata().setType(TRIAL_DESIGN);
        aFreshRequest.getMetadata().setTimestamp(DATETIME_FORMAT.format(now()));

    }

    @Test
    public void givenExistingRequest_whenProccessDesignArray_thenSuccess() {
        when(requestService.findByStatus(any())).thenReturn(Arrays.asList(aFreshRequest));

        subject.proccessDesignArray();

        verify(requestService).findByStatus(eq(submitted));
    }

    @Test
    public void givenExistingRequest_whenProccessCustomDesignArray_thenSuccess() {
        aFreshRequest.getMetadata().setType(CUSTON_DESIGN);
        aFreshRequest.getMetadata().setDesign("my-file.csv");
        ;
        when(requestService.findByStatus(any())).thenReturn(Arrays.asList(aFreshRequest));

        subject.proccessCustomDesignArray();

        verify(requestService).findByStatus(eq(processing_custom_design));
    }

    @Test
    public void givenOldRequest_whenIsOldRequest_thenReturnTrue() {
        Request object = new Request();
        object.getMetadata().setTimestamp("2020-08-19T14:13:45.000");

        System.out.println((object.getMetadata().getTimestamp()));

        assertThat(subject.isOldRequest(object)).isTrue();
    }

    @Test
    public void givenNewRequest_whenIsOldRequest_thenReturnFalse() {
        Request object = new Request();
        object.getMetadata().setTimestamp(DATETIME_FORMAT.format(now()));

        assertThat(subject.isOldRequest(object)).isFalse();
    }

    @Test
    public void givenExistingRequestInput_whenProccessCustomDesignArray_thenSuccess() {
        when(requestService.findByStatus(any())).thenReturn(Arrays.asList(aFreshRequest));

        subject.proccessCustomDesignArray();

        verify(requestService).findByStatus(eq(processing_custom_design));
    }

    @Test
    public void givenDefinedDesign_whenCreatePath_thenBuildPath() {
        Request object = new Request();
        object.setMetadata(new RequestMetadata());
        object.getMetadata().setType(TRIAL_DESIGN);
        object.getMetadata().setId("MY_ID123");

        Path result = subject.createPath(object);

        String expected = Path.of("src", "test", "resources", "example-designs", "MY_ID123",
                "MY_ID123_DesignArray.csv").toString();

        assertEquals(expected, result.toString());

    }

    @Test
    public void givenCustomDesign_whenCreatePath_thenGetFilePath() {
        Request object = new Request();
        object.getMetadata().setType(CUSTON_DESIGN);
        object.getMetadata().setDesign("testFile.csv");

        Path result = subject.createPath(object);

        String expected = Path.of("src", "test", "resources", "example-designs", "testFile.csv")
                .toString();
        assertEquals(expected, result.toString());
    }

    @Test
    public void givenOldRequest_whenVerifyRequestStatus_thenReturnTrueAndMarkAsExpired() {
        Request object = new Request();
        object.getMetadata().setId("RCBD_cimmyt_SD_0000");
        object.getMetadata().setTimestamp("2020-06-09T14:13:45.000");

        boolean result = subject.verifyRequestStatus(object);

        assertFalse(result);
        verify(requestService, times(1)).updateStatus(eq("RCBD_cimmyt_SD_0000"), eq(expired));
    }

    @Test
    public void givenCustomDesign_whenVerifyRequestStatus_thenReturnTrue() {
        aFreshRequest.getMetadata().setType(CUSTON_DESIGN);

        boolean result = subject.verifyRequestStatus(aFreshRequest);

        assertTrue(result);
    }

    @Test
    public void givenFailedAeoResponse_whenVerifyRequestStatus_thenReturnFalseAndMarkAsFailed() {

        AEOResponse aeoReturnObject = new AEOResponse();
        aeoReturnObject.setStatus(Arrays.asList(new AEOStatus()));
        aeoReturnObject.getStatus().get(0).setRequestStatus(failed);

        when(mockAeoClient.get(anyString(), any(), eq(AEOResponse.class)))
                .thenReturn(aeoReturnObject);

        boolean result = subject.verifyRequestStatus(aFreshRequest);

        assertFalse(result);
        verify(requestService, times(1)).updateStatus(eq("RCBD_cimmyt_SD_0000"), eq(failed));
    }

    @Test
    public void givenAeoNotResponding_whenVerifyRequestStatus_thenReturnFalse() {

        boolean result = subject.verifyRequestStatus(aFreshRequest);

        assertFalse(result);
    }

    @Test
    public void givenAeoResponse_whenVerifyRequestStatus_thenReturnTrue() {

        AEOResponse aeoReturnObject = new AEOResponse();
        aeoReturnObject.setStatus(Arrays.asList(new AEOStatus()));
        aeoReturnObject.getStatus().get(0).setRequestStatus(completed);

        when(mockAeoClient.get(anyString(), any(), eq(AEOResponse.class)))
                .thenReturn(aeoReturnObject);

        boolean result = subject.verifyRequestStatus(aFreshRequest);

        assertTrue(result);
    }

    @Test
    public void givenDesignArrayFileExists_whenProcessRequest_thenSuccess() {
        AEOResponse aeoReturnObject = new AEOResponse();
        aeoReturnObject.setStatus(Arrays.asList(new AEOStatus()));
        aeoReturnObject.getStatus().get(0).setRequestStatus(completed);

        when(requestService.findByStatus(any())).thenReturn(Arrays.asList(aFreshRequest));
        when(requestService.updateStatus(anyString(), any())).thenReturn(RequestStatus.completed);
        when(b4rApiClient.postRecords(anyString(), anyList(), any())).thenReturn(response);
        when(b4rApiClient.withUser(anyString())).thenReturn(b4rApiClient);
        when(parser.generateDesign(any(), any())).thenReturn(mockDesignArray);
        when(parser.parseDesignTable(any(), anyMap(), any())).thenReturn(Collections.emptyList());
        when(mockAeoClient.get(anyString(), any(), eq(AEOResponse.class)))
                .thenReturn(aeoReturnObject);

        subject.proccessDesignArray();

        verify(requestService).findByStatus(eq(submitted));
        verify(requestService).updateStatus(anyString(), eq(completed));
        verify(b4rApiClient).postRecords(anyString(), anyList(), any());
        verify(parser, times(1)).generateDesign(any(), any());
        verify(parser, times(1)).parseDesignTable(any(), anyMap(), any());

    }
}