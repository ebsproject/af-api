package org.ebs.services;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;

import org.ebs.model.AfRequest;
import org.ebs.model.repos.AfRequestRepository;
import org.ebs.services.to.Request;
import org.ebs.services.to.RequestFileDesignInput;
import org.ebs.services.to.RequestStatus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.convert.ConversionService;

@ExtendWith(MockitoExtension.class)
public class RequestServiceImplTest {

    @Mock
    private AfRequestRepository afRequestRepository;
    @Mock
    private ConversionService conversionService;
    @Mock
    private DPOService dpo;
    @Mock
    private AfRequest afReq;
    @Mock
    private Request req;

    private RequestService subject;

    @BeforeEach
    public void init() {
        subject = new RequestServiceImpl(afRequestRepository, conversionService, dpo);
    }

    @Test
    public void givenValidRequestInput_whenCreateRequest_thenSaveRequest() throws IOException {
        when(conversionService.convert(any(), eq(AfRequest.class)))
        .thenReturn(afReq);
        when(conversionService.convert(any(),eq(Request.class)))
        .thenReturn(req);
        when(dpo.processDesignRequest(any(), any()))
        .thenReturn(RequestStatus.submitted);
        when(afRequestRepository.save(afReq))
        .thenReturn(afReq);
        
        subject.createRequest(Stub.ofRequestInput());
        
        verify(afReq).setStatus(eq(RequestStatus.created));
        verify(afReq).setUuid(any());
        
        verify(afRequestRepository, times(2)).save(eq(afReq));
        verify(dpo).processDesignRequest(eq(req), any());
    }
    
    @Test
    public void givenValidRequestInput_whenCreateFileDesignRequest_thenSaveRequest() throws IOException {
        when(conversionService.convert(any(), eq(AfRequest.class))).thenReturn(afReq);
        when(conversionService.convert(any(), eq(Request.class))).thenReturn(new Request());
        
        subject.createFileDesignRequest(new RequestFileDesignInput());
        
        verify(afReq).setStatus(eq(RequestStatus.processing_custom_design));
        verify(afReq).setUuid(any());
        verify(afRequestRepository, times(1)).save(eq(afReq));
        verify(conversionService, times(2)).convert(any(), any());

    }
}