package org.ebs.services;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import org.ebs.model.AfProperty;
import org.ebs.model.AfPropertyConfig;
import org.ebs.model.AfPropertyMeta;
import org.ebs.model.AfPropertyRule;
import org.ebs.model.repos.AfPropertyConfigRepository;
import org.ebs.model.repos.AfPropertyMetaRepository;
import org.ebs.model.repos.AfPropertyRepository;
import org.ebs.model.repos.AfPropertyUiRepository;
import org.ebs.services.to.Property;
import org.ebs.services.to.PropertyMeta;
import org.ebs.services.to.PropertyRule;
import org.ebs.services.to.PropertyUi;
import org.ebs.util.Connection;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.PageRequest;

@ExtendWith(MockitoExtension.class)
public class PropertyServiceImplTest {

    @Mock
    AfPropertyRepository propertyRepository;
    @Mock
    AfPropertyUiRepository propertyUiRepository;
    @Mock
    AfPropertyConfigRepository propertyConfigRepository;
    @Mock
    ConversionService conversionService;
    @Mock
    AfProperty afProperty;
    @Mock
    AfPropertyConfig afPropertyConfig;
    @Mock
    AfPropertyMetaRepository afPropertyMetaRepository;

    private PropertyService subject;

    @BeforeEach
    public void init() {
        subject = new PropertyServiceImpl(propertyRepository, propertyConfigRepository,
                conversionService, afPropertyMetaRepository);
    }

    @Test
    public void whenPropertyExists_thenFindProperty() {
        when(propertyConfigRepository.findByPropertyIdAndConfigPropertyIdAndDeletedIsFalse(anyInt(),
                anyInt())).thenReturn(Optional.of(afPropertyConfig));

        subject.findProperty(1, 1);

        verify(propertyConfigRepository)
                .findByPropertyIdAndConfigPropertyIdAndDeletedIsFalse(anyInt(), anyInt());
        verify(conversionService).convert(any(), eq(Property.class));
    }

    @Test
    public void whenCatalogExists_thenFindCatalog() {
        when(propertyConfigRepository
                .findByPropertyIdAndConfigPropertyTypeAndDeletedIsFalse(anyInt(), anyString()))
                        .thenReturn(Collections.singleton(afPropertyConfig));

        subject.findCatalog(1, null);

        verify(propertyConfigRepository).findByPropertyIdAndConfigPropertyTypeAndDeletedIsFalse(
                anyInt(), eq("catalog_root"));
        verify(conversionService).convert(any(), eq(Property.class));
    }

    @Test
    public void givenPropertyNotExists_whenFindCatalogElements_thenReturnNull() {
        when(propertyRepository.findByIdAndDeletedIsFalse(any())).thenReturn(Optional.empty());

        assertThat(subject.findCatalogElements(new Property())).isNull();

        verify(propertyRepository).findByIdAndDeletedIsFalse(anyInt());
    }

    @Test
    public void givenPropertyIsCatalog_whenFindCatalogElements_thenReturnCatalogElements() {
        when(propertyRepository.findByIdAndDeletedIsFalse(anyInt()))
                .thenReturn(Optional.of(afProperty));
        when(afProperty.getType()).thenReturn("catalog_root");
        when(propertyConfigRepository.findByCriteria(any(), any(), nullable(SortInput.class),
                nullable(PageInput.class))).thenReturn(
                        new Connection<>(List.of(afPropertyConfig), PageRequest.of(1, 1), 1));

        assertThat(subject.findCatalogElements(new Property())).isNotEmpty();

        verify(propertyRepository).findByIdAndDeletedIsFalse(anyInt());
        verify(propertyConfigRepository, times(1)).findByCriteria(any(), any(),
                nullable(SortInput.class), nullable(PageInput.class));
        verify(propertyConfigRepository, times(0))
                .findByPropertyIdAndConfigPropertyTypeAndDeletedIsFalse(anyInt(),
                        eq("catalog_item"));
    }

    @Test
    public void givenPropertyIsNotCatalog_whenFindCatalogElements_thenReturnElements() {
        when(propertyRepository.findByIdAndDeletedIsFalse(anyInt()))
                .thenReturn(Optional.of(afProperty));
        when(afProperty.getType()).thenReturn("some type");
        when(propertyConfigRepository.findByCriteria(any(), any(), nullable(SortInput.class),
                nullable(PageInput.class))).thenReturn(
                        new Connection<>(List.of(afPropertyConfig), PageRequest.of(1, 1), 1));

        assertThat(subject.findCatalogElements(new Property())).isNotEmpty();

        verify(propertyRepository).findByIdAndDeletedIsFalse(anyInt());
        verify(propertyConfigRepository, times(0))
                .findByPropertyIdAndConfigPropertyTypeAndDeletedIsFalse(anyInt(), anyString());
        verify(propertyConfigRepository, times(1)).findByCriteria(any(), any(),
                nullable(SortInput.class), nullable(PageInput.class));
    }

    @Test
    public void whenPropertyUiExists_thenFindPropertyUi() {
        when(propertyConfigRepository.findByPropertyIdAndConfigPropertyIdAndDeletedIsFalse(anyInt(),
                anyInt())).thenReturn(Optional.of(afPropertyConfig));

        subject.findPropertyUi(1, 1);

        verify(propertyConfigRepository)
                .findByPropertyIdAndConfigPropertyIdAndDeletedIsFalse(anyInt(), anyInt());
        verify(conversionService).convert(any(), eq(PropertyUi.class));
    }

    @Test
    public void whenPropertyRulesExist_thenFindPropertyRules() {
        when(propertyConfigRepository.findByPropertyIdAndConfigPropertyIdAndDeletedIsFalse(anyInt(),
                anyInt())).thenReturn(Optional.of(afPropertyConfig));
        when(afPropertyConfig.getRules()).thenReturn(
                new HashSet<>(Arrays.asList(new AfPropertyRule(), new AfPropertyRule())));
        when(conversionService.convert(any(), any())).thenReturn(new PropertyRule(),
                new PropertyRule());

        assertThat(subject.findPropertyRules(1, 1)).size().isEqualTo(2);

        verify(propertyConfigRepository)
                .findByPropertyIdAndConfigPropertyIdAndDeletedIsFalse(anyInt(), anyInt());
        verify(conversionService, times(2)).convert(any(), eq(PropertyRule.class));
    }

    @Test
    public void whenPropertyMetaExist_thenFindPropertyMeta() {
        when(afPropertyMetaRepository.findByPropertyIdAndDeletedIsFalse(anyInt())).thenReturn(
                new HashSet<>(Arrays.asList(new AfPropertyMeta(), new AfPropertyMeta())));
        when(conversionService.convert(any(), any())).thenReturn(new PropertyMeta(),
                new PropertyMeta());

        assertThat(subject.findPropertyMeta(1)).size().isEqualTo(2);

        verify(afPropertyMetaRepository).findByPropertyIdAndDeletedIsFalse(anyInt());
        verify(conversionService, times(2)).convert(any(), eq(PropertyMeta.class));
    }
}