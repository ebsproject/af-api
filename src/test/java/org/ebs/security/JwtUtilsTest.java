package org.ebs.security;

import static org.assertj.core.api.Assertions.assertThat;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.interfaces.JWTVerifier;

import org.assertj.core.util.DateUtil;
import org.junit.jupiter.api.Test;

public class JwtUtilsTest {

    @Test
    public void aTest(){
        String secret = "mySecret";
        try {
            Algorithm algorithm = Algorithm.HMAC256(secret);
            String token = JWT.create()
                .withIssuer("obg.ebs.sg")
                .withClaim("email", "test@cgiar.org")
                .withClaim("userId", 1)
                .withExpiresAt(DateUtil.tomorrow())
                .sign(algorithm);

            algorithm = Algorithm.HMAC256(secret);
            JWTVerifier verifier = JWT.require(algorithm)
                .withIssuer("obg.ebs.sg")
                .build();

            DecodedJWT decodedToken = verifier.verify(token);

            assertThat(decodedToken.getClaim("userId").asInt())
                .isEqualTo(1);
            assertThat(decodedToken.getClaim("email").asString())
                .isEqualTo("test@cgiar.org");
            

        } catch (JWTCreationException exception){
            //Invalid Signing configuration / Couldn't convert Claims.
        }
    }

}