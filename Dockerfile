FROM gradle:6.8.3-jdk11 AS gradleBuild

WORKDIR /home/gradle/project

COPY --chown=gradle:gradle . /home/gradle/project

RUN gradle build --no-daemon --stacktrace

FROM openjdk:11-jre

ENV spring.datasource.url=jdbc:postgresql://ebs-af-db:5432/afdb
ENV spring.datasource.username=postgres
ENV spring.datasource.password=postgres
ENV spring.profiles.active=default
ENV ebs.sg.tenant.endpoint=https://ebs.cimmyt.org:8243/api-tnt/0.1/
ENV ebs.sg.b4rapi.endpoint=https://ebs.cimmyt.org:8243/api-bc-wheat/20.01/
ENV ebs.sg.aeo.endpoint=http://ebs-af-rafiki/v1/
ENV ebs.sg.b4rapi.jwt-secret=Vu3L8QoDPhrRYJ0cDCoVhVxMWvfzakiR
ENV ebs.sg.b4rapi.jwt-expiry=60

COPY --from=gradleBuild /home/gradle/project/build/libs/ebs-sg-af.jar ./app.jar

ENTRYPOINT ["java","-jar","/app.jar"]